<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Worldline India Pvt Ltd</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/worldline-bookmark-icon-44x44.png"
	type="image/x-icon" />
<!--[if lte IE 9]>
	  <script src="resources/js/html5shiv.js"></script>
	  <script src="resources/js/respond.js"></script>
	 <![endif]-->
<!-- <script type="text/javascript"
	src="/ccap/resources/js/formencoder/encoderAlgo.js"></script>
<script type="text/javascript"
	src="/ccap/resources/js/formencoder/formEncoder.js"></script> -->
<!-- <script type="text/javascript" src="/ccap/resources/js/jquery.bg.js"></script> -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/login-page-slider.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#password').bind("cut copy paste", function(e) {
			e.preventDefault();
		});
		var pathname = window.location.pathname;
		var queryString = window.location.search;
		//alert(queryString);
		if(queryString != "")
		{
			if(pathname == '${pageContext.request.contextPath}/login' || pathname == '${pageContext.request.contextPath}/')
			{
				$('#scriptErrMsg').hide();
			}
		}
		
		$('#login').attr('autocomplete', 'off');

		$('#loginBtn').click(function() {
			if ($('#loginIDText').val() != "" && $('#passwordText').val() != "") {
				$('#username').val($('#loginIDText').val());
				$('#password').val($('#passwordText').val());

				$('#loginIDText').val('');
				$('#passwordText').val('');

				$('#submit').click();
			} else {
				$('#scriptErrMsg').val('Incorrect Login Id and Password.');
				$('#scriptErrMsg').text('Incorrect Login Id and Password.');
				$('#scriptErrMsg').show();
			}
		});

	});
	$(document).keypress(function(e) {
		if (e.which == 13) {
			$('#loginBtn').click();
		}
	});
	window.history.forward();
	function noBack() {
		window.history.forward();
	}

	window.onload = function() {
		var pass = document.getElementById('password').value;
		pass.onpaste = function(e) {
			e.preventDefault();
		}
	}

	function hidePara() {
		$('#errMsg').hide();
		$('#infoMsg').hide();
		$('#scriptErrMsg').hide();
	}
</script>
<style>
.alert-danger {
	background-color: #f2dede;
	border-color: #ebccd1;
	color: #a94442;
}

.alert {
	border: 1px solid transparent;
	border-radius: 4px;
	font-size: 12px;
	margin: 0 15px 7px 9px;
	margin-bottom: 9px;
	padding: 7px;
	width: 324px;
}

p.ex {
	color: #ff0000;
	font-size: 12px;
}
</style>

</head>

<body onload="noBack();">



	<div class="container_slider">

		<div class="mid_part">
			<section id="content" style="min-height:200px;margin-top:10%">
			<form id="login" class="form-signin" style="display: none;"
				action="<c:url value='${request.contextPath}/login' />"
				method="post" autocomplete="off">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<div>
					<input id="username" name="username" class="form-control"
						onclick="hidePara()" autofocus="autofocus"
						placeholder="Enter Login ID" name="loginID" type="text" value=""
						autocomplete="off" />
				</div>
				<div>
					<input id="password" name="password" class="form-control"
						onclick="hidePara()" placeholder="Enter Password" name="password"
						type="text" value="" autocomplete="off" />
				</div>
				<div>
					<button id="submit" name="submit" class="btn1" type="submit"
						value="Login">Login</button>
				</div>
			</form>
			<form>
				<img src="${pageContext.request.contextPath}/resources/images/logo.png"
					style="width: 180px; margin: 0 0 12px -4px" />
					<div id="scriptErrMsg" class="alert alert-danger" role="alert" style="display: none;">
					</div>
				<c:if test="${not empty error}">
					<div id="errMsg" class="alert alert-info" role="alert">
								${error}						
					</div>
				</c:if>
				<c:if test="${not empty msg}">
					<div id="infoMsg" class="alert alert-info" role="alert">
								${msg}						
					</div>
				</c:if>

				<div>
					<input type="text" autocomplete="off" id="loginIDText"
						onclick="hidePara()" placeholder="Enter Login ID"
						cssClass="form-control" autofocus="autofocus" /> <span
						for="loginID" id="loginErr" class="error"></span>
				</div>
				<div>
					<input type="password" autocomplete="off" id="passwordText"
						onclick="hidePara()" placeholder="Enter Password"
						cssClass="form-control" /> <span for="password" id="pwdErr"
						class="error"></span>
				</div>
				<div>
					<button type="button" id="loginBtn" name="submit" class="btn1">Login</button>
				</div>
			</form>

			</section>
			<!-- content -->
		</div>

	</div>

	

</body>
</html>
