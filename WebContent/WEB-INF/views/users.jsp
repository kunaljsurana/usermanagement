<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page session="false"%>
<html>
<head>
<title>Bank Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="commonfile.jsp"></jsp:include>
<script type="text/javascript">
	$(document).ready(function() {
		var errorsMsg = "";
		$('#errMsg').hide();
		$('#saveBtn').click(function() {
			var errors = 0;

			if (errors > 0) {
				$('#errMsg').html(errorsMsg);
				$('#errMsg').show();
				errorsMsg = "";
			} else {
				var x = document.getElementById("saveBtn").value;
				var msg;
				if (x.localeCompare("Save") == 0) {
					msg = "Are you sure, You want to edit User?";
				} else {
					msg = "Are you sure, You want to Add User?";
				}
				if (confirm(msg) == true) {
					$('form#userForm').submit();
				}
			}
		});
	});
	function hidePara() {
		$('#errMsg').hide();
	}

	showConfirmation = function(title, message, success, cancel) {
		title = title ? title : 'Are you sure?';
		var modal = $("#main_confirmation");
		modal.find(".modal-title").html(title).end().find(".modal-body").html(
				message).end().modal({
			backdrop : 'static',
			keyboard : false
		}).on('hidden.bs.modal', function() {
			modal.unbind();
		});
		if (success) {
			$('form#userForm').submit();
		}
		if (cancel) {
			modal.one('click',
					'.modal-header .close, .modal-footer .btn-default', cancel);
		}
	};

	$(document).on(
			"click",
			".delete-event, .delete-all-event",
			function(event) {
				event.preventDefault();
				var self = $(this);
				var url = $(this).data('url');
				var success = function() {
					alert('window.location.href=url');
				}
				var cancel = function() {
					alert('Cancel');
				};
				if (self.data('confirmation')) {
					var title = self.data('confirmation-title') ? self
							.data('confirmation-title') : undefined;
					var message = self.data('confirmation');
					showConfirmation(title, message, success, cancel);
				} else {
					success();
				}
			});
</script>
</head>
<body>
	<div class="container">
		<%-- <c:url var="firstUrl" value="/banks/1" />
		<c:url var="lastUrl" value="/banks/${totalpage}" /> --%>
		<c:if
			test="${!fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
			<c:url var="addAction" value="/users/add"></c:url>
		</c:if>
		<c:if
			test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
			<c:url var="addAction" value="/users/edit"></c:url>
		</c:if>




		<br />
		<div class="panel panel-default row">
			<div class="panel-heading">
				<spring:message code="lbl.user.addNewUser"
					text="Add heading in property file with label lbl.user.addNewUser" />
			</div>
			<div class="panel-body">
				<form:form action="${addAction}" id="userForm" commandName="user"
					modelAttribute="user" method="post" cssClass="form-horizontal">
					<div id="errMsg" class="alert alert alert-danger" role="alert"></div>
					<form:errors cssClass="alert alert alert-danger" element="div" />
					<c:if test="${!empty user.id}">
						<div class="form-group" style="display: none; width: 0px">
							<div class="col-md-2">
								<form:label path="id" cssClass="control-label" cssStyle="">
									<spring:message code="lbl.id" text="id" />
								</form:label>
							</div>
							<div class="col-md-3">
								<form:input path="id" readonly="true" size="8" disabled="true"
									cssClass="form-control" />
								<form:hidden path="id" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<div class="col-md-3 ">
							<form:label path="firstname" cssClass="control-label">
								<spring:message text="firstname" code="lbl.user.firstname" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">
							<form:input path="firstname" cssClass="form-control"
								autocomplete="off" />
						</div>
						<div class="col-md-6">
							<form:errors path="firstname" cssClass="form-control customAlert alert-danger" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 ">
							<form:label path="lastname" cssClass="control-label">
								<spring:message text="lastname" code="lbl.user.lastname" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">
							<form:input path="lastname" cssClass="form-control"
								autocomplete="off" />
						</div>
						<div class="col-md-6">
							<form:errors path="lastname" cssClass="form-control customAlert alert-danger" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 ">
							<form:label path="emailid" cssClass="control-label">
								<spring:message text="emailid" code="lbl.user.emailid" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">
							<form:input path="emailid" cssClass="form-control"
								autocomplete="off" />
						</div>
						<div class="col-md-6">
							<form:errors path="emailid" cssClass="form-control customAlert alert-danger" />
						</div>
					</div>
					<c:if
						test="${!fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
						<div class="form-group">
							<div class="col-md-3 ">
								<form:label path="username" cssClass="control-label">
									<spring:message text="username" code="lbl.user.username" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
							<div class="col-md-3">
								<form:input path="username" cssClass="form-control"
									autocomplete="off" />
							</div>
							<div class="col-md-6">
								<form:errors path="username" cssClass="form-control customAlert alert-danger" />
							</div>
						</div>
					</c:if>
					<c:if
						test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
						<div class="form-group">
							<div class="col-md-3 ">
								<form:label path="username" cssClass="control-label">
									<spring:message text="username" code="lbl.user.username" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
							<div class="col-md-3">
								<form:input path="username" cssClass="form-control"
									autocomplete="off" readonly="true" />
							</div>
							<div class="col-md-6">
								<form:errors path="username" cssClass="form-control customAlert alert-danger" />
							</div>
						</div>
					</c:if>
					<c:if
						test="${!fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
						<div class="form-group">
							<div class="col-md-3 ">
								<form:label path="password" cssClass="control-label">
									<spring:message text="password" code="lbl.user.password" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
							<div class="col-md-3">
								<form:password path="password" cssClass="form-control"
									autocomplete="off" />
							</div>
							<div class="col-md-6">
								<form:errors path="password" cssClass="form-control customAlert alert-danger" />
							</div>
						</div>
					</c:if>
					<c:if
						test="${!fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
						<div class="form-group">
							<div class="col-md-3 ">
								<form:label path="repassword" cssClass="control-label">
									<spring:message text="repassword" code="lbl.user.repassword" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
							<div class="col-md-3">
								<form:password path="repassword" cssClass="form-control"
									autocomplete="off" />
							</div>
							<div class="col-md-6">
								<form:errors path="repassword" cssClass="form-control customAlert alert-danger" />
							</div>
						</div>
					</c:if>

					<c:if
						test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
						<input type="hidden" name="password" value="dummy" />
						<input type="hidden" name="repassword" value="dummy" />
					</c:if>

					<div class="form-group">
						<div class="col-md-3 ">
							<form:label path="roles" cssClass="control-label">
								<spring:message text="roleList" code="lbl.user.roleList" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">

							<form:select path="roles" cssClass="form-control" multiple="true">
								<form:options items="${roleList}" itemValue="id"
									itemLabel="role" />
							</form:select>
						</div>
						<div class="col-md-6">
							<form:errors path="roles" cssClass="form-control customAlert alert-danger" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3">
							<div class="checkbox">
								<form:label path="enabled" cssClass="control-label">
									<form:checkbox path="enabled" />
									<spring:message text="enabled" code="lbl.user.enabled" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="checkbox">
								<form:label path="accountNotExpired" cssClass="control-label">
									<form:checkbox path="accountNotExpired" />
									<spring:message text="accountNotExpired"
										code="lbl.user.accountNotExpired" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="checkbox">
								<form:label path="credentialNotExpired" cssClass="control-label">
									<form:checkbox path="credentialNotExpired" />
									<spring:message text="credentialNotExpired"
										code="lbl.user.credentialNotExpired" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="checkbox">
								<form:label path="accountNotLocked" cssClass="control-label">
									<form:checkbox path="accountNotLocked" />
									<spring:message text="accountNotLocked"
										code="lbl.user.accountNotLocked" />&nbsp;<span
										style="color: red;">*</span>
								</form:label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12" style="text-align: center;">
							<c:if
								test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
								<%-- <c:if test="${!empty bank.bankCode}"> --%>
								<input type="button" class="btn btn-info" id="saveBtn"
									class="btn btn-info"
									value="<spring:message  code="lbl.save" text="Save"/>" />
							</c:if>

							<%-- <c:if test="${empty bank.bankCode}"> --%>
							<c:if
								test="${!fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit') }">
								<input type="button" class="btn btn-info" id="saveBtn"
									value="<spring:message code="lbl.user.addBtn" text="Add"/>" />&nbsp;&nbsp;&nbsp;
									&nbsp;
									<input class="btn btn-info" type="reset" value="Reset"
									onclick="hidePara()">
							</c:if>
						</div>
					</div>
				</form:form>
			</div>
		</div>

		<br>

		<div class="panel panel-default row">
			<div class="panel-heading" style="text-align: center;">
				<spring:message code="lbl.user.dataTable.heading" text="List" />
			</div>

			<display:table class="table table-striped table-bordered displayTag"
				decorator="com.awl.displaytag.DisplayTagDecorator" export="false"
				name="userList" pagesize="10" partialList="true" size="totalsize"
				requestURI="${pageContext.request.contextPath}/users">
				<display:column property="firstname" title="First Name"
					style="width:80px" />
				<display:column property="lastname" title="Last Name"
					style="width:300px" />
				<display:column property="emailid" title="Email"
					style="width:300px" />
				<display:column property="username" title="Login ID"
					style="width:200px" />
				<display:column property="roles" title="Roles" style="width:180px" />
				<display:column property="enabled" title="Enabled"
					style="width:180px" />
				<display:column property="accountNotExpired" title="Account Expired"
					style="width:180px" />
				<display:column property="credentialNotExpired"
					title="Credential Expired" style="width:180px" />
				<display:column property="accountNotLocked" title="Account Locked"
					style="width:180px" />
				<display:column property="created" title="Created"
					style="width:200px" />
				<display:column property="updated" title="Last Updated"
					style="width:180px" />
				<display:column property="editUser" title="Edit" />
				<display:column property="disabledUser" title="Disable" />
			</display:table>
		</div>
	</div>
</body>
</html>
