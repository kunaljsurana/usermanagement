<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page session="false"%>
<html>
<head>
<title>Bank Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="commonfile.jsp"></jsp:include>
<script type="text/javascript">
	$(document).ready(function() {
		var errorsMsg = "";
		$('#errMsg').hide();
		$('#saveBtn').click(function() {
			var errors = 0;

			if (errors > 0) {
				$('#errMsg').html(errorsMsg);
				$('#errMsg').show();
				errorsMsg = "";
			} else {
				var x = document.getElementById("saveBtn").value;
				var msg;
				if (x.localeCompare("Save") == 0) {
					msg = "Are you sure, You want to edit Function?";
				} else {
					msg = "Are you sure, You want to Add Function?";
				}
				if (confirm(msg) == true) {
					$('form#functionForm').submit();
				}
			}
		});
	});
	function hidePara() {
		$('#errMsg').hide();
	}

	showConfirmation = function(title, message, success, cancel) {
		title = title ? title : 'Are you sure?';
		var modal = $("#main_confirmation");
		modal.find(".modal-title").html(title).end().find(".modal-body").html(
				message).end().modal({
			backdrop : 'static',
			keyboard : false
		}).on('hidden.bs.modal', function() {
			modal.unbind();
		});
		if (success) {
			$('form#functionForm').submit();
		}
		if (cancel) {
			modal.one('click',
					'.modal-header .close, .modal-footer .btn-default', cancel);
		}
	};

	$(document).on(
			"click",
			".delete-event, .delete-all-event",
			function(event) {
				event.preventDefault();
				var self = $(this);
				var url = $(this).data('url');
				var success = function() {
					alert('window.location.href=url');
				}
				var cancel = function() {
					alert('Cancel');
				};
				if (self.data('confirmation')) {
					var title = self.data('confirmation-title') ? self
							.data('confirmation-title') : undefined;
					var message = self.data('confirmation');
					showConfirmation(title, message, success, cancel);
				} else {
					success();
				}
			});
</script>
</head>
<body>
	<div class="container">
		<%-- <c:url var="firstUrl" value="/banks/1" />
		<c:url var="lastUrl" value="/banks/${totalpage}" /> --%>
		<c:url var="addAction" value="/functions/add"></c:url>
		<form:form action="${addAction}" id="functionForm" commandName="function"
			modelAttribute="function" method="post" cssClass="form-inline">

			<c:if test="${!empty function.id}">
				<div class="row" style="display: none; width: 0px">
					<div class="col-md-2">
						<form:label path="id" cssClass="control-label" cssStyle="">
							<spring:message code="lbl.id" text="id" />
						</form:label>
					</div>
					<div class="col-md-3">
						<form:input path="id" readonly="true" size="8" disabled="true"
							cssClass="form-control" />
						<form:hidden path="id" />
					</div>
				</div>
			</c:if>
			<br />
			<div class="panel panel-default row">
				<div class="panel-heading">
					<spring:message code="lbl.function.addNewFunction"
						text="Add heading in property file with label lbl.function.addNewFunction" />
				</div>
				<div class="panel-body">
					<div id="errMsg" class="alert alert alert-danger" role="alert"></div>

					<div class="row">
						<div class="col-md-3 ">
							<form:label path="functionName" cssClass="control-label">
								<spring:message text="functionName" code="lbl.function.functionName" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">
							<form:input path="functionName" cssClass="form-control"
								autocomplete="off" />
						</div>
						<div class="col-md-6">
							<form:errors path="functionName" cssClass="alert alert-danger" />
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-3 ">
							<form:label path="url" cssClass="control-label">
								<spring:message text="url" code="lbl.function.url" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">
							<form:input path="url" cssClass="form-control"
								autocomplete="off" />
						</div>
						<div class="col-md-6">
							<form:errors path="url" cssClass="alert alert-danger" />
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-3 ">
							<form:label path="group" cssClass="control-label">
								<spring:message text="functionGroupList" code="lbl.function.group" />&nbsp;<span
									style="color: red;">*</span>
							</form:label>
						</div>
						<div class="col-md-3">

							<form:select path="group" cssClass="form-control"  items="${functionGroupList}" itemValue="id" itemLabel="groupName">
							</form:select>
						</div>
						<div class="col-md-6">
							<form:errors path="group" cssClass="alert alert-danger"  />
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12" style="text-align: center;">
							<c:if
								test="${fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit')}">
								<%-- <c:if test="${!empty bank.bankCode}"> --%>
								<input type="button" class="btn btn-info" id="saveBtn"
									class="btn btn-info"
									value="<spring:message  code="lbl.save" text="Save"/>" />
							</c:if>

							<%-- <c:if test="${empty bank.bankCode}"> --%>
							<c:if
								test="${!fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'edit') or fn:contains(requestScope['javax.servlet.forward.servlet_path'], 'add')}">
								<input type="button" class="btn btn-info" id="saveBtn"
									value="<spring:message code="lbl.function.addBtn" text="Add"/>" />&nbsp;&nbsp;&nbsp;
									&nbsp;
									<input class="btn btn-info" type="reset" value="Reset"
									onclick="hidePara()">
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</form:form>
		<br>

		<div class="panel panel-default row">
			<div class="panel-heading" style="text-align: center;">
				<spring:message code="lbl.functionDataTable.heading" text="List" />
			</div>

			<display:table class="table table-striped table-bordered displayTag"
				decorator="com.awl.displaytag.DisplayTagDecorator"
				export="false" name="functionList" pagesize="10" partialList="true"
				size="totalsize"
				requestURI="${pageContext.request.contextPath}/functions">
				<display:column property="functionName" title="Function"
					style="width:80px" />
				<display:column property="url" title="URL"
					style="width:300px" />
				<display:column property="group" title="Function Group"
					style="width:180px" />
				<display:column property="created" title="Created"
					style="width:200px" />
				<display:column property="updated" title="Last Updated"
					style="width:180px" />
				<display:column property="editFunction" title="Edit" />
				<display:column property="deleteFunction" title="Delete" />
			</display:table>
		</div>
	</div>
</body>
</html>
