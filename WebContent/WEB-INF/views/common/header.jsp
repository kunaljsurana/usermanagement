<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<script>
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>
<style>
#loged{
	margin-bottom: 10px;
}
a {
	color: #333;
}

#nav {
	margin: 0;
	padding: 7px 6px 0;
	/* background: #2e6da4 url(img/gradient.png) repeat-x 0 -110px; */
	line-height: 100%;
	border-radius: 2em;
	-webkit-border-radius: 2em;
	-moz-border-radius: 2em;
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .4);
	-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .4);
}

#nav li {
	margin: 0 5px;
	padding: 0 0 8px;
	float: left;
	position: relative;
	list-style: none;
}

/* main level link */
#nav a {
	font-weight: bold;
	color: #2e6da4;
	text-decoration: none;
	display: block;
	padding: 8px 20px;
	margin: 0;
	border-radius: 1.6em;
	-webkit-border-radius: 1.6em;
	-moz-border-radius: 1.6em;
	text-shadow: 0 1px 1px rgba(0, 0, 0, .3);
}

#nav a:hover {
	background: #2e6da4;
	color: #2e6da4;
	border-radius: 1.6em;
	-webkit-border-radius: 1.6em;
	-moz-border-radius: 1.6em;
}

/* main level link hover */
#nav .current a, #nav li:hover>a {
	color: #323232;
	border-top: solid 1px #f8f8f8;
	-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .2);
	-moz-box-shadow: 0 1px 1px rgba(0, 0, 0, .2);
	box-shadow: 0 1px 1px rgba(0, 0, 0, .2);
}

/* sub levels link hover */
#nav ul li:hover a, #nav li:hover li a {
	background: none;
	border: none;
	color: #666;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	border-radius:0px;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
}

#nav ul a:hover {
	background: #2e6da4 /* url(img/gradient.png) */ repeat-x 0 -100px !important;
	color: #fff !important;
	border-radius : 0 px;
	-webkit-border-radius: 0;
	-moz-border-radius: 0;
	text-shadow: 0 1px 1px rgba(0, 0, 0, .1);
}

/* dropdown */
#nav li:hover>ul {
	display: block;
}

/* level 2 list */
#nav ul {
	display: none;
	margin: 0;
	padding: 0;
	width: 185px;
	position: absolute;
	top: 35px;
	left: 0;
	border: solid 1px #b4b4b4;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .3);
	-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .3);
	box-shadow: 0 1px 3px rgba(0, 0, 0, .3);
}

#nav ul li {
	float: none;
	margin: 0;
	padding: 0;
}

#nav ul a {
	font-weight: normal;
	text-shadow: 0 1px 0 #fff;
}

/* level 3+ list */
#nav ul ul {
	left: 181px;
	top: -3px;
}

/* rounded corners of first and last link */
#nav ul li:first-child>a {
	-webkit-border-top-left-radius: 9px;
	-moz-border-radius-topleft: 9px;
	-webkit-border-top-right-radius: 9px;
	-moz-border-radius-topright: 9px;
	border-top-left-radius:9px;
	border-top-right-radius:9px;
}

#nav ul li:last-child>a {
	-webkit-border-bottom-left-radius: 9px;
	-moz-border-radius-bottomleft: 9px;
	-webkit-border-bottom-right-radius: 9px;
	-moz-border-radius-bottomright: 9px;
	border-bottom-left-radius:9px;
	border-bottom-right-radius:9px;
}

/* clearfix */
#nav:after {
	content: ".";
	display: block;
	clear: both;
	visibility: hidden;
	line-height: 0;
	height: 0;
}
</style>
<!-- </head> -->
<div class="header">
		<div class="container">
			<div class="row">
				<div class="navbar-header ">
					<button aria-controls="navbar" aria-expanded="false"
						data-target="#navbar" data-toggle="collapse"
						class="navbar-toggle collapsed" type="button">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<img src="<%=request.getContextPath()%>/resources/images/logo.png"
						width="160px" style="padding: 2px">
				</div>
	
				<div class="navbar-collapse collapse" id="loged">
		
					<div style="float: right; margin: 9px 38px 0 0">
						<span class="nav navbar-nav" style="color: rgb(46, 109, 164)">
							<b>Last Login Date & Time :</b> ${logindate} &nbsp; Welcome,
							${author}
						</span>
		
		
						<!-- <li><a href="#"
										onclick="urlPostAction('N','/changePassword');"> <span
											class="glyphicon glyphicon-pencil"> Change Password </span>
									</a></li> -->
						<a onclick="noBack();" href='<c:url value="/logout" />'> <span
							class="nav navbar-nav">&nbsp; &nbsp; Logout</span>
						</a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="span6">
					<ul id="nav" style="width: 100%;">
						<c:if test="${empty sessionScope.menuMap}">
							<jsp:forward page="/index"></jsp:forward>
						</c:if>
						<c:forEach var="menuList" items="${sessionScope.menuMap}">
								<c:if test="${fn:length(menuList.value) gt 0 }">
								
									<c:choose>
										<c:when test="${fn:length(menuList.value) == 1 and menuList.key == menuList.value.iterator().next().functionName}">											
											<c:forEach var="submenu" items="${menuList.value}">												
												<li class="current">
													<a  href='<c:url value="${submenu.url}" />'>${submenu.functionName}</a>
												</li>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<li class="current">
												<a data-toggle="dropdown" href="#">${menuList.key}<b class="caret bottom-up"></b></a>
												<ul class="dropdown-menu bottom-up pull-right">	
													<c:forEach var="submenu" items="${menuList.value}">
														<li><a href='<c:url value="${submenu.url}" />'>${submenu.functionName}</a></li>
													</c:forEach>
												</ul>
											</li>
										</c:otherwise>
									</c:choose>
								</c:if>
								
						</c:forEach>
						
					
						<%-- <li class="current"><a href="<c:url value="/index" />">Home</a></li>
						<li class="current"><a data-toggle="dropdown" href="#">CCA
								Admin<b class="caret bottom-up"></b>
						</a>
							<ul class="dropdown-menu bottom-up pull-right">
								<li><a href='<c:url value="/banks" />'> Bank</a></li>
								<li><a href='<c:url value="/encryptions" />'> Upload
										Encryption</a></li>
								<li><a href='<c:url value="/routers" />'>e-Host</a></li>
								<li><a href='<c:url value="/masterbins" />'> Master
										Bin</a></li>
								<li><a href='<c:url value="/userdetails" />'> Card
										Details</a></li>
								<li><a href='<c:url value="/cardDeregister" />'> Card
										De-register</a></li>
								<li><a href='<c:url value="/userControlle/1" />'> Card
										Control Status</a></li>
							</ul></li>
						<li class="current"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Report<b
								class="caret bottom-up"></b></a>
							<ul class="dropdown-menu bottom-up pull-right">
								<li><a href='<c:url value="/otpsms" />'> OTP SMS Report</a></li>
									<li><a href='<c:url value="/confirmationsms" />'>
										Confirmation SMS</a></li>
								<li><a href='<c:url value="/cardRegister" />'> Card
										Register Report</a></li>
								<li><a href='<c:url value="/cardTxnReport" />'> Transaction
										Report </a></li>
								<li><a href='<c:url value="/cardRegister/1" />'> Card
										Register</a></li>
								<li><a href='<c:url value="/cardTxn" />'> Card
										Transaction</a></li>
							</ul></li> --%>
					</ul>
				</div>
			</div>
	</div>
</div>