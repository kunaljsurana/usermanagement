<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<style>
#section {
	min-height: 800px;
}
</style>
<body>

	<div id="header">
		<!-- Header Tiles -->
		<tiles:insertAttribute name="header" />
	</div>


	<div id="section">
		<!-- Body Tiles -->
		<tiles:insertAttribute name="body" />
	</div>

	<div id="footer">
		<tiles:insertAttribute name="footer" />
	</div>
	<!-- footer Tiles -->

</body>
</html>