<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Access Denied - 403</title>

<!-- Bootstrap -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<jsp:include page="commonfile.jsp"></jsp:include>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			
			<div class="col-md-6" style="margin-top:30px; text-align: center">
				<img alt="403"
					src='<c:url value="/resources/images/access-denied.png"></c:url>'>
				<h1>403 - Access is Denied</h1>
				<c:choose>
					<c:when test="${empty username}">
						<h2>You do not have permission to access this page!</h2>
					</c:when>
					<c:otherwise>
						<h2>
							Username : ${username} <br /> You do not have permission to
							access this page!
						</h2>
					</c:otherwise>
				</c:choose>

			</div>
			<div class="col-md-3"></div>
		</div>
	</div>



</body>
</html>