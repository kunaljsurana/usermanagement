<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Worldline India Pvt Ltd</title>
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/images/worldline-bookmark-icon-44x44.png"
	type="image/x-icon" />
<!--[if lte IE 9]>
	  <script src="resources/js/html5shiv.js"></script>
	  <script src="resources/js/respond.js"></script>
	 <![endif]-->
<!-- <script type="text/javascript"
	src="/ccap/resources/js/formencoder/encoderAlgo.js"></script>
<script type="text/javascript"
	src="/ccap/resources/js/formencoder/formEncoder.js"></script> -->
<!-- <script type="text/javascript" src="/ccap/resources/js/jquery.bg.js"></script> -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/login-page-slider.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		window.history.forward();
		function noBack() {
			window.history.forward();
		}

		window.onload = function() {

			hidePara();
			
		}

		function hidePara() {
			$('#infoMsg').hide();
			$('#scriptErrMsg').hide();
		}
	});
</script>
<style>
.alert-danger {
	background-color: #f2dede;
	border-color: #ebccd1;
	color: #a94442;
}

.alert {
	border: 1px solid transparent;
	border-radius: 4px;
	font-size: 12px;
	margin: 0 15px 7px 9px;
	margin-bottom: 9px;
	padding: 7px;
	width: 324px;
}

#forgetPwdForm {
	text-align: left;
}

p.ex {
	color: #ff0000;
	font-size: 12px;
}
</style>

</head>

<body onload="noBack();">

	<c:url var="addAction" value="/forgetPwd/reset"></c:url>

	<div class="container_slider" >
		<div  style="min-height: 200px; margin-top: 10%">
			<img
				src="${pageContext.request.contextPath}/resources/images/logo.png"
				style="width: 180px; margin: 0 0 12px -4px" />
			<form:form action="${addAction}" id="forgetPwdForm"
				commandName="pwdResetToken" modelAttribute="pwdResetToken"
				method="post" cssClass="form-horizontal">
				<spring:hasBindErrors name="pwdResetToken">
						<c:if test="${not empty errors.allErrors}">
							<div id="errMsg" class="col-md-12 alert alert-danger"
								role="alert">
								<ul>
									<c:forEach items="${errors.allErrors}" var="error">
										<li><spring:message message="${error}" /></li>
									</c:forEach>
								</ul>
							</div>
						</c:if>
				</spring:hasBindErrors>
				<c:if test="${not empty error}">
					<div id="errMsg" class="alert alert-danger" role="alert">
								${error}						
					</div>
				</c:if>
				<c:if test="${not empty msg}">
					<div id="errMsg" class="alert alert-info" role="alert">
								${msg}						
					</div>
				</c:if>
				<form:errors cssClass="alert alert alert-danger" element="div" />
				<div class="form-group">
					<form:label path="emailid" cssClass="col-md-6"
						cssStyle="">
						<spring:message text="Email" ></spring:message>
					</form:label>
					<br /><br />
					<form:input path="emailid" cssClass="form-control col-md-12"
						autocomplete="off"  place-holder="Email ID"/>
						</div>
				<div class="form-group" style="text-align: center;">
					<form:button name="submit" class="btn btn-default navbar-btn" >Reset Password</form:button>
				</div>
			</form:form>
		</div>

	</div>



</body>
</html>
