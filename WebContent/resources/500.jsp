<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page  isErrorPage="true"  %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="<%=request.getContextPath()%>/resources/js/jquery-1.10.2.js"></script>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Server Internal Error - 500</title>
<script type="text/javascript">

        $(document).ready(

        function() {
                var ctx = "${pageContext.request.contextPath}";

                $('#homeBtn').click(function() {
                        location.href = ctx + "/index";
                });

        });
</script>
<!-- Bootstrap -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			
			<div class="col-md-6" style="margin-top:30px; text-align: center">
				<img alt="403"
					src='<c:url value="/resources/images/500-error.png"></c:url>'>
				<h1>Something went technically wrong !!<br/> 500 - Server Internal Error!!</h1>
				<br/>
				<input type="button" class="btn btn-info" id="homeBtn"
										name="homeBtn" value="<spring:message  text="Home"/>" />
				<%-- <h5>Exception is ${pageContext.exception}</h5>
				<br/>
				<h5>Message is ${pageContext.exception.message}</h5>
				<br/>
				<div>
				<b>Stack trace:</b>
				<br/>
					<c:forEach var="trace" items="${pageContext.exception.stackTrace}">
						<p>${trace}</p>
					</c:forEach>
				</div>
				<br/>
				<div>
				<b>Cause:</b>
				<br/>
					<c:forEach var="causeTrace" items="${pageContext.exception.cause.stackTrace}">
						<p>
						${pageContext.exception.cause}
						<br/>
						${causeTrace}
						</p>
					</c:forEach>
				</div>
				<br/> --%>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>



</body>
</html>