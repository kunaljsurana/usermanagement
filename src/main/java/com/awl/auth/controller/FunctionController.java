package com.awl.auth.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.awl.auth.editors.FunctionGroupEditor;
import com.awl.auth.model.Function;
import com.awl.auth.model.FunctionGroup;
import com.awl.auth.service.FunctionGroupService;
import com.awl.auth.service.FunctionService;
import com.awl.auth.validators.FunctionValidator;

@Controller
public class FunctionController {

	private static final Logger logger = LoggerFactory
			.getLogger(FunctionController.class);

	@Autowired
	@Qualifier("functionGroupService")
	FunctionGroupService functionGroupService;

	@Autowired
	@Qualifier("functionService")
	FunctionService functionService;

	Integer totalSize = 0;

	private HashMap<String, FunctionGroup> functionGroupMap;

	@RequestMapping(value = "/functions", method = RequestMethod.GET)
	public String listFunction( HttpServletRequest request, Model model) {
		Integer pageNumber = 1;

		try{
			pageNumber = Integer.parseInt(request.getParameter("d-49653-p"));
		}catch(Exception e)
		{
			pageNumber = 1;
		}

		model.addAttribute("function", new Function());
		model.addAttribute("functionList",
				this.functionService.findAll((pageNumber - 1) * /*WebConstant.MAX_NO_RECORD*/ 10 , 10 /*WebConstant.MAX_NO_RECORD*/));

		if(totalSize == 0)
		{
			totalSize = (int) this.functionService.count();
		}		
		model.addAttribute("totalsize", totalSize);

	

		return "function";
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		logger.debug("entering initBinder method in Function  Controller");
		binder.registerCustomEditor(FunctionGroup.class, "group", new FunctionGroupEditor(functionGroupMap));
		binder.addValidators(new FunctionValidator());
		logger.debug("leaving initBinder method");
	}


	@RequestMapping(value = "/functions/add", method = RequestMethod.POST)
	public String addFunction(@Validated @ModelAttribute("function")  Function function, BindingResult result, Model model) {
		List<Function> functions = this.functionService.findAll(-1, -2);
		
		if(result.hasErrors())
		{
			model.addAttribute("totalsize", totalSize);
			return "function";
		}
		if (function!=null && validate(functions, function)) {
			result.addError(new FieldError("function", "function", "Functionname already exist"));
			model.addAttribute("totalsize", totalSize);
			return "function";
		} else {
			//logger.debug("addFunction - id:"+function.getId());
			if (function.getId() == 0) {
				functionService.add(function);
				totalSize = totalSize + 1;
			} else {
				functionService.update(function);
			}
		}
		return "redirect:/functions";
	}

	private boolean validate(List<Function> functions, Function function) {
		int id = function.getId();
		if (id == 0) {
			for (Function b : functions) {
				if (b.getFunctionName().equalsIgnoreCase(function.getFunctionName())) {
					return true;
				}
			}
		} else {
			for (Function b : functions) {
				if (b.getId() != id) {
					if (b.getFunctionName().equalsIgnoreCase(function.getFunctionName())) {
						return true;
					}
				}
			}
		}
		return false;
	}



	@RequestMapping("/functions/remove/{id}")
	public String disableFunction(@PathVariable("id") int id) {
		this.functionService.remove(id);
		totalSize = totalSize - 1;
		return "redirect:/functions";
	}

	@RequestMapping("/functions/edit/{id}")
	public String editFunction(@PathVariable("id") int id, Model model) {
		Function function = this.functionService.getById(id);
		logger.debug("inside editFunction - Group:"+function.getGroup());
		model.addAttribute("function", function);
		model.addAttribute("functionList", this.functionService.findAll(-1, -1));
		model.addAttribute("totalsize", totalSize);
		return "function";
	}

	@ModelAttribute("functionGroupList")
	public List<FunctionGroup> populateFunctionGroupList() {
		functionGroupMap = new HashMap<String,FunctionGroup>() ; 
		List<FunctionGroup> functionGroupList = functionGroupService.findAll(-1, -1);
		for(FunctionGroup functionGroup : functionGroupList)
		{
			functionGroupMap.put(String.valueOf(functionGroup.getId()),functionGroup);
		}
		logger.debug("Functions in  populateFunctionList:"+functionGroupMap);

		return functionGroupList;
	}
}
