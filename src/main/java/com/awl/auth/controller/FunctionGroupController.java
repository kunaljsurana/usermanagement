package com.awl.auth.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.awl.auth.model.FunctionGroup;
import com.awl.auth.service.FunctionGroupService;

@Controller
public class FunctionGroupController {

	private static final Logger logger = LoggerFactory
			.getLogger(FunctionGroupController.class);

	@Autowired
	@Qualifier("functionGroupService")
	FunctionGroupService functionGroupService;
	
	Integer totalSize = 0;


	@RequestMapping(value = "/functionGroups", method = RequestMethod.GET)
	public String listFunctionGroup( HttpServletRequest request, Model model) {
		Integer pageNumber = 1;

		try{
			pageNumber = Integer.parseInt(request.getParameter("d-49653-p"));
		}catch(Exception e)
		{
			pageNumber = 1;
		}

		model.addAttribute("functionGroup", new FunctionGroup());
		model.addAttribute("functionGroupList",
				this.functionGroupService.findAll((pageNumber - 1) * /*WebConstant.MAX_NO_RECORD*/ 10 , 10 /*WebConstant.MAX_NO_RECORD*/));

		if(totalSize == 0)
		{
			totalSize = (int) this.functionGroupService.count();
		}		
		model.addAttribute("totalsize", totalSize);

	

		return "functionGroup";
	}




	@RequestMapping(value = "/functionGroups/add", method = RequestMethod.POST)
	public String addFunctionGroup(@ModelAttribute("functionGroup") FunctionGroup functionGroup, BindingResult result, Model model) {
		List<FunctionGroup> functionGroups = this.functionGroupService.findAll(-1, -2);
		if(functionGroup.getGroupName().length() > 50)
		{
			result.addError(new FieldError("functionGroup", "groupName", "Function Group Name Length should not exceed 50 characters"));
			model.addAttribute("totalsize", totalSize);
			return "functionGroup";
		}
		if (functionGroup!=null && validate(functionGroups, functionGroup)) {
			result.addError(new FieldError("functionGroup", "groupName", "Function Group already exist"));
			model.addAttribute("totalsize", totalSize);
			return "functionGroup";
		} else {
			logger.debug("addFunctionGroup - id:"+functionGroup.getId());
			if (functionGroup.getId() == 0) {
				functionGroupService.add(functionGroup);
				totalSize = totalSize + 1;
			} else {
				functionGroupService.update(functionGroup);
			}
		}
		return "redirect:/functionGroups";
	}

	private boolean validate(List<FunctionGroup> functionGroups, FunctionGroup functionGroup) {
		int id = functionGroup.getId();
		if (id == 0) {
			for (FunctionGroup b : functionGroups) {
				if (b.getGroupName().equalsIgnoreCase(functionGroup.getGroupName())) {
					return true;
				}
			}
		} else {
			for (FunctionGroup b : functionGroups) {
				if (b.getId() != id) {
					if (b.getGroupName().equalsIgnoreCase(functionGroup.getGroupName())) {
						return true;
					}
				}
			}
		}
		return false;
	}



	@RequestMapping("/functionGroups/remove/{id}")
	public String disableFunctionGroup(@PathVariable("id") int id) {
		this.functionGroupService.remove(id);
		totalSize = totalSize - 1;
		return "redirect:/functionGroups";
	}

	@RequestMapping("/functionGroups/edit/{id}")
	public String editFunctionGroup(@PathVariable("id") int id, Model model) {
		FunctionGroup functionGroup = this.functionGroupService.getById(id);
		model.addAttribute("functionGroup", functionGroup);
		model.addAttribute("functionGroupList", this.functionGroupService.findAll(-1, -1));
		model.addAttribute("totalsize", totalSize);
		return "functionGroup";
	}


}
