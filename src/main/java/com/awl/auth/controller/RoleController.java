package com.awl.auth.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.awl.auth.editors.FunctionEditor;
import com.awl.auth.model.Function;
import com.awl.auth.model.Role;
import com.awl.auth.service.FunctionService;
import com.awl.auth.service.RoleService;
import com.awl.auth.validators.RoleValidator;

@Controller
public class RoleController {

	private static final Logger logger = LoggerFactory
			.getLogger(RoleController.class);

	@Autowired
	@Qualifier("functionService")
	FunctionService functionService;

	@Autowired
	@Qualifier("roleService")
	RoleService roleService;

	Integer totalSize = 0;

	private HashMap<String, Function> functionMap;

	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public String listRole( HttpServletRequest request, Model model) {
		Integer pageNumber = 1;

		try{
			pageNumber = Integer.parseInt(request.getParameter("d-49653-p"));
		}catch(Exception e)
		{
			pageNumber = 1;
		}

		model.addAttribute("role", new Role());
		model.addAttribute("roleList",
				this.roleService.findAll((pageNumber - 1) * /*WebConstant.MAX_NO_RECORD*/10, 10/*WebConstant.MAX_NO_RECORD*/));

		if(totalSize == 0)
		{
			totalSize = (int) this.roleService.count();
		}		
		model.addAttribute("totalsize", totalSize);

	

		return "role";
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		logger.debug("entering initBinder method in Role Controller");
		binder.registerCustomEditor(Set.class, "functions", new FunctionEditor(Set.class,functionMap));
		binder.addValidators(new RoleValidator());
		logger.debug("leaving initBinder method");
	}


	@RequestMapping(value = "/roles/add", method = RequestMethod.POST)
	public String addRole(@ModelAttribute("role") @Validated Role role, BindingResult result, Model model) {
		logger.debug("inside addRole");
		List<Role> roles = this.roleService.findAll(-1, -2);
		if(result.hasErrors())
		{
			logger.error("inside addRole: error");
			model.addAttribute("totalsize", totalSize);
			return "role";
		}
		if (role!=null && validate(roles, role)) {
			logger.debug("Rolename already exist");
			result.addError(new FieldError("role", "role", "Rolename already exist"));
			model.addAttribute("totalsize", totalSize);
			return "role";
		} else {
			logger.debug("addRole - id:"+role.getId());
			if (role.getId() == 0) {
				role.setRole("ROLE_"+role.getRole().toUpperCase());
				roleService.add(role);
				totalSize = totalSize + 1;
			} else {
				logger.debug("Updating role");
				roleService.update(role);
			}
		}
		return "redirect:/roles";
	}

	private boolean validate(List<Role> roles, Role role) {
		int id = role.getId();
		if (id == 0) {
			for (Role b : roles) {
				if (b.getRole().equalsIgnoreCase(role.getRole())) {
					return true;
				}
			}
		} else {
			for (Role b : roles) {
				if (b.getId() != id) {
					if (b.getRole().equalsIgnoreCase(role.getRole())) {
						return true;
					}
				}
			}
		}
		return false;
	}



	@RequestMapping("/roles/remove/{id}")
	public String disableRole(@PathVariable("id") int id) {
		this.roleService.remove(id);
		totalSize = totalSize - 1;
		return "redirect:/roles";
	}

	@RequestMapping("/roles/edit/{id}")
	public String editRole(@PathVariable("id") int id, Model model) {
		Role role = this.roleService.getById(id);
		model.addAttribute("role", role);
		model.addAttribute("roleList", this.roleService.findAll(-1, -1));
		model.addAttribute("totalsize", totalSize);
		return "role";
	}

	@ModelAttribute("functionList")
	public List<Function> populateRoleList() {
		functionMap = new HashMap<String,Function>() ; 
		List<Function> functionList = functionService.findAll(-1, -1);
		for(Function function : functionList)
		{
			functionMap.put(String.valueOf(function.getId()),function);
		}
		logger.debug("Roles in  populateRoleList:"+functionMap);

		return functionList;
	}
}
