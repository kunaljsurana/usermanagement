package com.awl.auth.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.awl.auth.model.Function;
import com.awl.auth.model.FunctionGroup;
import com.awl.auth.model.PasswordResetToken;
import com.awl.auth.model.Role;
import com.awl.auth.model.User;
import com.awl.auth.service.FunctionGroupService;
import com.awl.auth.service.MyUserDetailsService;
import com.awl.auth.service.RoleService;
import com.awl.utility.MD5Generation;
import com.awl.utility.MailSender;

/**
 * @author Kunal Surana
 *
 */
@Controller
@SessionAttributes({ "author", "logindate","menuMap"  })
public class LoginController {
	// private static String loginDate = null;
	// static {
	// loginDate = MD5Generation.dateToString();
	// }
	private static final Logger logger = LoggerFactory
			.getLogger(LoginController.class);


	@Autowired
	@Qualifier("myUserDetailsService")
	MyUserDetailsService userService;

	@Autowired
	@Qualifier("functionGroupService")
	FunctionGroupService functionGroupService;

	@Autowired
	@Qualifier("roleService")
	RoleService roleService;

	@Autowired
	@Qualifier("springMailSender")
	MailSender mailSender;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String executeSecurity(ModelMap model, Principal principal,HttpSession session) {


		String name = principal.getName();
		model.addAttribute("author", name);
		model.addAttribute("logindate", MD5Generation.dateToString());
		model.addAttribute("message", "Welcome to CCA Control");	

		Object obj = session.getAttribute("menuMap");
		if(obj==null)
		{
			User user = userService.getUser(name);
			Set<Role> roles = user.getRoles();
			ArrayList<Function> functionList = new ArrayList<Function>();
			for(Role role : roles)
			{
				functionList.addAll(roleService.getFunctions(role));
			}

			List<FunctionGroup> headings = functionGroupService.list();

			LinkedHashMap<String, TreeSet<Function>> menuMap = new LinkedHashMap<String, TreeSet<Function>>();
			for(FunctionGroup heading : headings)
			{
				String str = heading.getGroupName();
				menuMap.put(str, new TreeSet<Function>());
			}

			for(Function func : functionList)
			{
				Set<Function> subMenu = menuMap.get(func.getGroup().getGroupName());
				if(subMenu!=null)
				{
					subMenu.add(func);
				}
			}


			logger.debug("User :"+user.getUsername()+" has Role :"+roles+" and has access to following functions :"+functionList);
			logger.debug("User :"+user.getUsername()+" will have following menu :"+menuMap);

			System.out.println("User :"+user.getUsername()+" has Role :"+roles.toString()+" and has access to following functions :"+functionList.toString());
			System.out.println("User :"+user.getUsername()+" will have following menu :"+menuMap);
			model.addAttribute("menuMap", menuMap);
		}
		else
		{
			logger.debug("Menu already loaded while login");
		}
		return "welcome";

	}



	@RequestMapping(value = {"/","/login"}, method = RequestMethod.GET)
	public String login(@RequestParam(value = "logout", required = false) String logout,@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "expired", required = false) String expired,@RequestParam(value = "sessionExpired", required = false) String sessionExpired,
			ModelMap model,HttpServletRequest request,HttpSession session) {
		if(logout!=null)
		{
			logger.debug("User :"+ session.getAttribute("author")+" has been logged out successfully");
			model.addAttribute("msg", "You've been logged out successfully.");
		}
		if (error != null) {
			model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}
		if (expired != null) {
			model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}
		if(sessionExpired!=null){
			logger.debug("Session has been expired for user :"+session.getAttribute("author"));
			model.addAttribute("msg", "Your session has expired. Please login again.");
		}
		logger.debug("leaving login GET");
		return "login";
	}

	@RequestMapping(value = {"/login"}, method = RequestMethod.POST)
	public String processLogin(ModelMap model,HttpServletRequest request,HttpSession session) {

		return "index";
	}



	private String getErrorMessage(HttpServletRequest request, String key) {
		// TODO Auto-generated method stub

		Exception exception = 
				(Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		}else if(exception instanceof SessionAuthenticationException) {
			if("Maximum sessions of 1 for this principal exceeded".equals(exception.getMessage()))
			{
				error = "User is already logged in from some system.";
			}
		}else if(exception instanceof LockedException) {
			error = exception.getMessage();
		}else if (exception instanceof AccountExpiredException) {
			error = exception.getMessage();
		}else if (exception instanceof DisabledException) {
			error = exception.getMessage();
		}else if (exception instanceof CredentialsExpiredException) {
			error = exception.getMessage();
		}else{
			error = "Invalid username and password!";
		}
		return error;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model, HttpSession session,Principal principal) {
		String name = principal.getName();
		//session.removeAttribute("author");
		//session.removeAttribute("logindate");
		//session.invalidate();
		logger.debug("in Logout Method");
		return "redirect:login";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(ModelMap model, Principal principal, HttpServletRequest req) {
		String name = principal.getName();
		model.addAttribute("username", name);
		logger.debug("User :"+name+" attempted to access unauthorized URL.");
		return "403";
	}


	@RequestMapping(value = "/forgetPwd/reset", method = RequestMethod.POST)
	public String sendResetLink(@ModelAttribute("pwdResetToken") @Validated PasswordResetToken resetToken,BindingResult result ,HttpServletRequest request, ModelMap model) {
		logger.debug("Entering sendResetLink");
		if(result.hasErrors())
		{
			logger.error("inside sendResetLink: error");
			return "forgetPwd";
		}
		String email = resetToken.getEmailid();
		logger.debug("Username / Email: "+email);
		User user = userService.findUserByEmail(email);
		if (user == null) {
			model.addAttribute("error", "User with this email does not exist");
			return "forgetPwd";
		}
		if(user.getEmailid()==null || user.getEmailid().isEmpty())
		{
			model.addAttribute("error", "Email not present for user");
			return "forgetPwd";
		}

		String token = UUID.randomUUID().toString();
		userService.createPasswordResetTokenForUser(user, token);
		String protocol ="http";
		if(request.isSecure())
		{
			protocol = "https";
		}

		String appUrl = 
				protocol + "://" + request.getServerName() + 
				":" + request.getServerPort() + 
				request.getContextPath();
		mailSender.sendResetTokenEmail(email, appUrl, user, token);
		model.addAttribute("msg", "Password reset link mail has been sent to your email id");
		return "forgetPwd";

	}

	@RequestMapping(value = "/forgetPwd", method = RequestMethod.GET)
	public String forgetPasswordPage(ModelMap model, Principal principal,HttpSession session) {


		PasswordResetToken prt = new PasswordResetToken();

		model.addAttribute("pwdResetToken", prt);

		return "forgetPwd";

	}
	
	@RequestMapping(value = "/forgetPwd/changePassword", method = RequestMethod.GET)
	public String showChangePasswordPage(ModelMap model, @RequestParam("id") long id, @RequestParam("token") String token) {


		 PasswordResetToken passToken = userService.getPasswordResetToken(token);
		 
		
		    User user = passToken.getUser();
		    if (passToken == null || user.getId() != id) {
		        model.addAttribute("error", "Invalid Token to change password. Please try again to reset your password.");
		        return "redirect:login";
		    }
		 
		    Calendar cal = Calendar.getInstance();
		    if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
		    	model.addAttribute("error", "Your link to reset password has been expired or has been used once. Please try again to reset your password.");
		    	 return "redirect:login";
		    }
		    
		    userService.updateForUnusableResetToken(passToken);
		    
		    logger.info("User to redirect for update :"+user);
		    
		    Authentication auth = new UsernamePasswordAuthenticationToken(
		      user, null, userService.loadUserByUsername(user.getUsername()).getAuthorities());
		    SecurityContextHolder.getContext().setAuthentication(auth);
		    
		    User usr = new User();
		    model.addAttribute("user", usr);
		    model.addAttribute("msg", null);
		 
		    return "updatePassword";


	}
	
	
	@RequestMapping(value = "/forgetPwd/savePwd", method = RequestMethod.POST)
	public String savePassword(@ModelAttribute("user") @Validated User userForm,BindingResult result , ModelMap model) {
	    User userToUpdate = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    
	    logger.info("Password to update:" +userForm.getPassword() );
	    logger.debug("Changing password for user id :"+userToUpdate);
	    
	    //User userToUpdate = this.userService.getById(user.getId());
	    
	    logger.debug("Changing password for user :"+userToUpdate.getEmailid());
	    
	    PasswordEncoder encoder = new BCryptPasswordEncoder();
	    String encodePwd = encoder.encode(userForm.getPassword());
	    logger.debug("Encoded Password :"+encodePwd);
	    
	    userToUpdate.setPassword(encodePwd);
	    userToUpdate.setUpdated(new Date());
		userService.update(userToUpdate);
		
		model.addAttribute("msg", "Password has been updated.");
		
		
		return "updatePassword";
	}
}