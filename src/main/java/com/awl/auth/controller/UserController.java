package com.awl.auth.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.awl.auth.editors.RoleEditor;
import com.awl.auth.model.Role;
import com.awl.auth.model.User;
import com.awl.auth.service.MyUserDetailsService;
import com.awl.auth.service.RoleService;
import com.awl.auth.validators.UserValidator;

@Controller
public class UserController {

	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);

	@Autowired
	@Qualifier("myUserDetailsService")
	MyUserDetailsService userService;

	@Autowired
	@Qualifier("roleService")
	RoleService roleService;

	Integer totalSize = 0;

	private HashMap<String, Role> roleMap;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String listUser( HttpServletRequest request, Model model) {
		Integer pageNumber = 1;

		try{
			pageNumber = Integer.parseInt(request.getParameter("d-49653-p"));
		}catch(Exception e)
		{
			pageNumber = 1;
		}

		model.addAttribute("user", new User());
		model.addAttribute("userList",
				this.userService.findAll((pageNumber - 1) * /*WebConstant.MAX_NO_RECORD*/10,10  /*WebConstant.MAX_NO_RECORD*/));

		if(totalSize == 0)
		{
			totalSize = (int) this.userService.count();
		}		
		model.addAttribute("totalsize", totalSize);

		/*if (this.userService.count() % WebConstant.MAX_NO_RECORD == 0)
			model.addAttribute("totalpage", (this.userService.count() / WebConstant.MAX_NO_RECORD));
		else
			model.addAttribute("totalpage", (this.userService.count() / WebConstant.MAX_NO_RECORD) + 1);*/



		return "user";
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		logger.debug("entering initBinder method in User Controller");
		binder.registerCustomEditor(Set.class, "roles", new RoleEditor(Set.class,roleMap));
		binder.addValidators(new UserValidator());

		logger.debug("leaving initBinder method");
	}


	@RequestMapping(value = "/users/add", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") @Validated User user, BindingResult result, Model model) {
		List<User> users = this.userService.findAll(-1, -2);
		if(result.hasErrors())
		{
			model.addAttribute("totalsize", totalSize);
			return "user";
		}
		if (user!=null && validate(users, user)) {
			result.addError(new FieldError("username", "username", "Username already exist"));
			model.addAttribute("totalsize", totalSize);
			return "user";
		} else {
			//logger.debug("addUser - id:"+user.getId());
			if (user.getId() == 0) {
				PasswordEncoder encoder = new BCryptPasswordEncoder();
				user.setPassword(encoder.encode(user.getPassword()));
				userService.add(user);
				totalSize = totalSize + 1;
			} else {
				User userToUpdate = this.userService.getById(user.getId());
				userToUpdate.setUsername(user.getUsername());
				userToUpdate.setFirstname(user.getFirstname());
				userToUpdate.setLastname(user.getLastname());
				userToUpdate.setEnabled(user.isEnabled());
				userToUpdate.setAccountNotExpired(user.isAccountNotExpired());
				userToUpdate.setCredentialNotExpired(user.isCredentialNotExpired());
				userToUpdate.setAccountNotLocked(user.isAccountNotLocked());
				userToUpdate.setRoles(user.getRoles());
				userService.update(userToUpdate);
			}
		}
		return "redirect:/users";
	}


	@RequestMapping(value = "/users/edit", method = RequestMethod.POST)
	public String editUser(@ModelAttribute("user") @Validated User user, BindingResult result, Model model) {
		if(result.hasErrors())
		{
			model.addAttribute("totalsize", totalSize);
			return "user";
		}

		//logger.debug("addUser - id:"+user.getId());

		User userToUpdate = this.userService.getById(user.getId());
		userToUpdate.setUsername(user.getUsername());
		userToUpdate.setFirstname(user.getFirstname());
		userToUpdate.setLastname(user.getLastname());
		userToUpdate.setEnabled(user.isEnabled());
		userToUpdate.setAccountNotExpired(user.isAccountNotExpired());
		userToUpdate.setCredentialNotExpired(user.isCredentialNotExpired());
		userToUpdate.setAccountNotLocked(user.isAccountNotLocked());
		userToUpdate.setRoles(user.getRoles());
		userService.update(userToUpdate);


		return "redirect:/users";
	}




	private boolean validate(List<User> users, User user) {
		int id = user.getId();
		if (id == 0) {
			for (User b : users) {
				if (b.getUsername().equalsIgnoreCase(user.getUsername())) {
					return true;
				}
				if (b.getEmailid().equalsIgnoreCase(user.getEmailid())) {
					return true;
				}
			}
		} else {
			for (User b : users) {
				if (b.getId() != id) {
					if (b.getUsername().equalsIgnoreCase(user.getUsername())) {
						return true;
					}
					if (b.getEmailid().equalsIgnoreCase(user.getEmailid())) {
						return true;
					}
				}
			}
		}
		return false;
	}



	@RequestMapping("/users/disable/{id}")
	public String disableUser(@PathVariable("id") int id) {
		User user = this.userService.getById(id);
		user.setEnabled(false);
		this.userService.update(user);
		totalSize = totalSize - 1;
		return "redirect:/users";
	}

	@RequestMapping("/users/edit/{id}")
	public String editUser(@PathVariable("id") int id, Model model) {
		User user = this.userService.getById(id);
		user.setRoles(this.userService.getRoles(id));
		model.addAttribute("user", user);
		model.addAttribute("userList", this.userService.findAll(-1, -1));
		model.addAttribute("totalsize", totalSize);
		return "user";
	}

	@ModelAttribute("roleList")
	public List<Role> populateRoleList() {
		roleMap = new HashMap<String,Role>() ; 
		List<Role> roleList = roleService.findAll(-1, -1);
		for(Role role : roleList)
		{
			roleMap.put(String.valueOf(role.getId()),role);
		}
		logger.debug("Roles in  populateRoleList:"+roleMap);

		return roleList;
	}
}
