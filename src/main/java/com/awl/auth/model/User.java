package com.awl.auth.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "Users" )
public class User implements Serializable {

	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = 5410504963402467397L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotNull @Size(max=50)
	private String firstname;
	@NotNull @Size(max=50)
	private String lastname;
	@NotNull @Size(max=60)
	private String password;
	@NotNull @Size(max=50)
	private String username;
	
	@NotNull @Size(max=100)
	@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	private String emailid;
	
	@Transient  
	private String repassword;
	
	@Column(nullable = false, columnDefinition = "TINYINT", length = 1) 
	private boolean enabled ;	
	
	@Column(name = "accountExpired" , nullable = false, columnDefinition = "TINYINT", length = 1) 
	private boolean accountNotExpired ;
	
	@Column(name = "accountLocked" , nullable = false, columnDefinition = "TINYINT", length = 1) 
	private boolean accountNotLocked ;
	
	@Column(name = "credentialExpired" , nullable = false, columnDefinition = "TINYINT", length = 1) 
	private boolean credentialNotExpired ;
		
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinTable(
	        name="UserRoles",
	        joinColumns=
	            @JoinColumn(name="userid" , nullable = false , updatable = false),
	        inverseJoinColumns=
	            @JoinColumn(name="roleid", nullable = false , updatable = false)
	    )
	private Set<Role> roles;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	

	public boolean isAccountNotExpired() {
		return accountNotExpired;
	}

	public void setAccountNotExpired(boolean accountNotExpired) {
		this.accountNotExpired = accountNotExpired;
	}

	public boolean isAccountNotLocked() {
		return accountNotLocked;
	}

	public void setAccountNotLocked(boolean accountNotLocked) {
		this.accountNotLocked = accountNotLocked;
	}

	public boolean isCredentialNotExpired() {
		return credentialNotExpired;
	}

	public void setCredentialNotExpired(boolean credentialNotExpired) {
		this.credentialNotExpired = credentialNotExpired;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	
	

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{" + getUsername() + ":" + getRoles() + "}"; 
	}
	
	



}