package com.awl.auth.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FunctionGroups")
public class FunctionGroup implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7938304048985039582L;
	@Id
	private int id;
	@Column(name = "groupname")
	private String groupName;
	@Column
	private Timestamp created;
	@Column
	private Timestamp updated;
	
	/*@OneToMany(fetch = FetchType.LAZY , cascade = CascadeType.MERGE)
	@JoinColumn(name = "groupid")
	private Set<Function> functions;*/
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getUpdated() {
		return updated;
	}
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.groupName;
	}
	
}
