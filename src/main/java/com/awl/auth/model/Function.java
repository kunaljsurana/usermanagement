package com.awl.auth.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Functions")
public class Function implements Serializable,Comparable<Function>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5311442158672978596L;
	@Id
	private int id;
	@Column(name = "functionname") @NotNull @Size(max=50)
	private String functionName;
	@Column @NotNull @Size(max=100) @Pattern(regexp = "^/.*")
	private String url;
	@Column
	private Timestamp created;
	@Column
	private Timestamp updated;
	
	@ManyToOne
    @JoinColumn(name="groupid") @NotNull
	private FunctionGroup group;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "functions")
	private Set<Role> roles;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getUpdated() {
		return updated;
	}
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	public FunctionGroup getGroup() {
		return group;
	}
	public void setGroup(FunctionGroup group) {
		this.group = group;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return functionName;
	}
	
	@Override
	public int compareTo(Function paramT) {
		// TODO Auto-generated method stub
		String funcName =  paramT.getFunctionName();
		return this.functionName.compareTo(funcName);
	}
	
	
	
}
