package com.awl.auth.model;

import java.util.Arrays;


public class Matcher {
	private String name;
	private String []roleInfo;
	
	public Matcher(String name, String[] roleInfo) {
		super();
		this.name = name + "**/**";
		this.roleInfo = roleInfo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getRoleInfo() {
		return roleInfo;
	}
	public void setRoleInfo(String[] roleInfo) {
		this.roleInfo = roleInfo;
	}
	
	
	@Override
	public String toString() {
		return "Matcher [name=" + name + ", roleInfo="
				+ Arrays.toString(roleInfo) + "]";
	}
	
	


}

