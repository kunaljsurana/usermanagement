package com.awl.auth.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.awl.auth.model.Role;


@Component
public class RoleValidator implements Validator {

	public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
		return Role.class.isAssignableFrom(clazz);
	}
	public void validate(final Object target, Errors errors) 
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role", "error.role.role");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "error.role.description");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "functions", "error.role.functions");
		
	}
}
