package com.awl.auth.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.awl.auth.model.User;


@Component
public class UserValidator implements Validator {

	public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
		return User.class.isAssignableFrom(clazz);
	}
	public void validate(final Object target, Errors errors) 
	{
		User user = (User) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstname", "error.user.firstname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "error.user.lastname");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailid", "error.user.emailid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.user.username");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.user.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "repassword", "error.user.repassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "roles", "error.user.roles");
		

		
		if(user.getPassword()!=null)
		{
			if(!user.getPassword().equals(user.getRepassword()))
			{
				errors.reject("error.user.mismatchedPassword");
			}
		}


	}
}
