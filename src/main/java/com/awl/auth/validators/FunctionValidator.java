package com.awl.auth.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.awl.auth.model.Function;


@Component
public class FunctionValidator implements Validator {

	public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
		return Function.class.isAssignableFrom(clazz);
	}
	public void validate(final Object target, Errors errors) 
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "functionName", 	"error.function.functionName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url", 			"error.function.url");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "group",		 	"error.function.group");
		
	}
}
