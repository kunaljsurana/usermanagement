package com.awl.auth.service;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.awl.auth.dao.RoleDao;
import com.awl.auth.model.Function;
import com.awl.auth.model.Role;
import com.awl.common.service.GenericServiceImpl;

@Component("roleService")
public class RoleServiceImpl extends GenericServiceImpl<Role> implements RoleService{
	
	
	private static final Logger logger = LoggerFactory
			.getLogger(RoleServiceImpl.class);

	
	@Autowired
	@Qualifier("roleDao")
	RoleDao roleDao;

	@Override
	public Set<Function> getFunctions(Role r) {
		// TODO Auto-generated method stub
		return roleDao.getFunctions(r);
	}
	
	@Override
	public Role getById(int id) {
		// TODO Auto-generated method stub
		
		Role r = super.getById(id);
		r.setFunctions(roleDao.getFunctions(r));
		return r;
	}
	
	
	
	@Override
	public List<Role> findAll(int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		List<Role> roleList = super.findAll(firstResult, maxResults);
		for(Role role : roleList){
			role.setFunctions(getFunctions(role));
			
		}
		logger.debug("Role List in find all :"+roleList);
		return roleList;
	}
	
	
}
