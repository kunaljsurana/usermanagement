package com.awl.auth.service;

import java.util.List;

import com.awl.auth.model.Function;
import com.awl.auth.model.Matcher;
import com.awl.common.service.GenericService;

public interface FunctionService  extends GenericService<Function>{
	public List<Matcher> getUrlMatchers();
}
