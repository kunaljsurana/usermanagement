package com.awl.auth.service;

import java.util.List;
import java.util.Set;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.awl.auth.model.PasswordResetToken;
import com.awl.auth.model.Role;
import com.awl.auth.model.User;
import com.awl.common.service.GenericService;

public interface MyUserDetailsService extends UserDetailsService, GenericService<User> {
	public User getUser(String username);
	public Set<Role> getRoles(Integer id);
	public List<User> findAll(int firstResult, int maxResults) ;
	public User findUserByEmail(String email);
	public void createPasswordResetTokenForUser(User user, String token);
	public PasswordResetToken getPasswordResetToken(String token);
	public void updateForUnusableResetToken(PasswordResetToken prt);
}
