package com.awl.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.awl.auth.dao.FunctionDao;
import com.awl.auth.model.Function;
import com.awl.auth.model.Matcher;
import com.awl.common.service.GenericServiceImpl;

@Component("functionService")
public class FunctionServiceImpl extends GenericServiceImpl<Function> implements
		FunctionService {
	
	@Autowired
	private FunctionDao functionDao;
	
	
	@Override
	public List<Matcher> getUrlMatchers() {
		// TODO Auto-generated method stub
		return functionDao.getUrlMatchers();
	}
	
	
	@Override
	public Function getById(int id) {
		// TODO Auto-generated method stub
		
		Function f = super.getById(id);
		f.setGroup(functionDao.getFunctionsGroup(f.getId()));
		return f;
	}
	
}
