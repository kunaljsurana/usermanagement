package com.awl.auth.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.awl.auth.dao.PasswordResetTokenDAO;
import com.awl.auth.dao.UserDAO;
import com.awl.auth.model.PasswordResetToken;
import com.awl.auth.model.Role;
import com.awl.common.service.GenericServiceImpl;

@Component("myUserDetailsService")
@Transactional(readOnly = true)
public class MyUserDetailsServiceImpl extends GenericServiceImpl<com.awl.auth.model.User> implements MyUserDetailsService {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private PasswordResetTokenDAO passwordResetTokenDAO;
	
	private static final Logger logger = LoggerFactory
			.getLogger(MyUserDetailsServiceImpl.class);


	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		System.out.println("Username :"+login);
		com.awl.auth.model.User domainUser = userDAO.findByUserName(login);
		
	/*	boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;*/
		logger.debug("Username :"+domainUser.getUsername());
		logger.debug("account expired :"+domainUser.isAccountNotExpired());
		logger.debug("credential expired :"+domainUser.isCredentialNotExpired());
		logger.debug("locked :"+domainUser.isAccountNotLocked());
		logger.debug("enabled :"+domainUser.isEnabled());
		return new User(domainUser.getUsername(), domainUser.getPassword(), domainUser.isEnabled(), domainUser.isAccountNotExpired(),
				domainUser.isCredentialNotExpired(), domainUser.isAccountNotLocked(), getAuthorities(domainUser));
	}

	public Collection<? extends GrantedAuthority> getAuthorities(com.awl.auth.model.User domainUser) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Set<Role> roles =  domainUser.getRoles();
		logger.debug("getAuthorities Roles :"+roles);
		for (Role role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getRole()));
		}

		return authorities;

	}

	@Override
	public com.awl.auth.model.User getUser(String username) {
		return userDAO.findByUserName(username);
	}

	@Override
	public Set<Role> getRoles(Integer id) {
		return userDAO.getRoles(id);
	}

	@Override
	public List<com.awl.auth.model.User> findAll(int firstResult, int maxResults) {
		//logger.debug("User List in find all");
		List<com.awl.auth.model.User> userList = super.findAll(firstResult, maxResults);
		for(com.awl.auth.model.User user : userList){
			/*user.setRoles(getRoles(user.getId()));*/
			user.getRoles();
		}
		logger.debug("User List in find all :"+userList);
		return userList;
	}

	@Override
	public com.awl.auth.model.User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userDAO.findUserByEmail(email);
	}

	@Transactional
	@Override
	public void createPasswordResetTokenForUser(com.awl.auth.model.User user,
			String token) {
		// TODO Auto-generated method stub
		logger.debug("saving reset token");
		
		PasswordResetToken prt = new PasswordResetToken();
		prt.setEmailid(user.getEmailid());
		prt.setToken(token);
		prt.setUser(user);
		
		
		Date oldDate = new Date(); // oldDate == current time
	    Date newDate = new Date(oldDate.getTime() + TimeUnit.HOURS.toMillis(24));
		
		prt.setExpiryDate(newDate);
		passwordResetTokenDAO.add(prt);		
		logger.debug("Reset Token :"+prt.toString());
	}
	
	@Transactional
	@Override
	public void updateForUnusableResetToken(PasswordResetToken prt)
			 {
		// TODO Auto-generated method stub
		logger.debug("updating reset token");
		prt.setExpiryDate(new Date());
		passwordResetTokenDAO.update(prt);		
		logger.debug("Reset Token updated :"+prt.toString());
	}

	@Override
	public PasswordResetToken getPasswordResetToken(String token) {
		// TODO Auto-generated method stub
		return passwordResetTokenDAO.getPasswordResetToken(token);
	}
	
	
	
	
	



}