package com.awl.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.awl.auth.dao.FunctionGroupDao;
import com.awl.auth.model.FunctionGroup;
import com.awl.common.service.GenericServiceImpl;

@Component("functionGroupService")
public class FunctionGroupServiceImpl extends GenericServiceImpl<FunctionGroup> implements
		FunctionGroupService{
	
	@Autowired
	@Qualifier("functionGroupDao")
	FunctionGroupDao functionGroupDao;

	@Override
	public List<FunctionGroup> list() {
		// TODO Auto-generated method stub
		return functionGroupDao.findAll();
	}
	
}
