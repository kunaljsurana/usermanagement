package com.awl.auth.service;

import java.util.List;
import java.util.Set;

import com.awl.auth.model.Function;
import com.awl.auth.model.Role;
import com.awl.common.service.GenericService;

public interface RoleService  extends GenericService<Role>{
	
	public Set<Function> getFunctions(Role r);
	public Role getById(int id);
	public List<Role> findAll(int firstResult, int maxResults);
}
