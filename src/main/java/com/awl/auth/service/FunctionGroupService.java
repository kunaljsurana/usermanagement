package com.awl.auth.service;

import java.util.List;

import com.awl.auth.model.FunctionGroup;
import com.awl.common.service.GenericService;

public interface FunctionGroupService  extends GenericService<FunctionGroup>{
	public List<FunctionGroup> list();
}
