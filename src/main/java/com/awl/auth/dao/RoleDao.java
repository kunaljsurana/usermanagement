package com.awl.auth.dao;

import java.util.Set;

import com.awl.auth.model.Function;
import com.awl.auth.model.Role;
import com.awl.common.dao.GenericDAO;

public interface RoleDao extends GenericDAO<Role> {

	public Set<Function> getFunctions(Role r);

}
