package com.awl.auth.dao;

import com.awl.auth.model.FunctionGroup;
import com.awl.common.dao.GenericDAO;


public interface FunctionGroupDao extends GenericDAO<FunctionGroup> {

}
