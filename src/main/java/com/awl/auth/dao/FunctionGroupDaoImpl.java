package com.awl.auth.dao;

import org.springframework.stereotype.Component;

import com.awl.auth.model.FunctionGroup;
import com.awl.common.dao.GenericDAOImpl;

@Component("functionGroupDao")
public class FunctionGroupDaoImpl extends GenericDAOImpl<FunctionGroup> implements FunctionGroupDao{

}
