package com.awl.auth.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.awl.auth.model.Function;
import com.awl.auth.model.FunctionGroup;
import com.awl.auth.model.Matcher;
import com.awl.auth.model.Role;
import com.awl.common.dao.GenericDAOImpl;
@Component("functionDao")
public class FunctionDaoImpl extends GenericDAOImpl<Function> implements FunctionDao{
	
	private static final Logger logger = LoggerFactory
			.getLogger(FunctionDaoImpl.class);
	
	@PersistenceContext
	private EntityManager manager;

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Matcher> getUrlMatchers() {
		// TODO Auto-generated method stub
		Session session = manager.unwrap(org.hibernate.Session.class);
		Criteria criteria = session.createCriteria(Function.class);
		ArrayList<Matcher> matcher = new ArrayList<Matcher>();
		List<Function> list = criteria.list();
		
		for(Function f : list)
		{
			
			ArrayList<String> roles = new ArrayList<String>();
			for(Role r : f.getRoles())
			{
				roles.add(r.getRole().substring(5));
			}
			
			Matcher m = new Matcher(f.getUrl(),roles.toArray(new String[roles.size()]));
			matcher.add(m);
		}
		return matcher;
	}


	@Override
	public FunctionGroup getFunctionsGroup(int i) {
		// TODO Auto-generated method stub
		try {
			
			Session session = manager.unwrap(org.hibernate.Session.class);
			Function function = (Function) session.get(Function.class,  i);
			FunctionGroup group =  function.getGroup();
			
	
			if (group !=null ) {
				return group;
			} 
			return null;
		} catch (RuntimeException re) {
			logger.error("in getFunctions"+re, re);
			throw re;
		}
	}

}
