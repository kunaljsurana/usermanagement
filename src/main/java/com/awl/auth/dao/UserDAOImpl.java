package com.awl.auth.dao;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.awl.auth.model.Role;
import com.awl.auth.model.User;
import com.awl.auth.model.UserAttempts;
import com.awl.common.dao.GenericDAOImpl;

@Component("userDAO")
public class UserDAOImpl extends GenericDAOImpl<User>  implements UserDAO {
	private static final Logger logger = LoggerFactory
			.getLogger(UserDAOImpl.class);

	@PersistenceContext
	private EntityManager manager;


	@Value(value = "#{'${login.MAX_ATTEMPTS}'}")
	private  String MAX_ATTEMPTS;

	public User findByUserName(String username) {
		/*logger.debug("User instance");*/
		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("username",username ));

			@SuppressWarnings("unchecked")
			List<User> users = criteria.list();



			/*List<User> users = manager
					.createQuery("SELECT c FROM User c WHERE c.login LIKE :login")
					.setParameter("login", username)
					.getResultList();*/
			if (users.size() > 0) {
				User user = users.get(0);
				logger.debug("User with Roles :"+user.getRoles());
				return user;
			} else {
				return null;
			}
		} catch (Exception re) {
			logger.error("user", re);
			throw re;
		}
	}

	@Override
	public Set<Role> getRoles(Integer id) {
		// TODO Auto-generated method stub
		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			User user = (User) session.get(User.class,id);
			Set<Role> roleSet = user.getRoles();


			if (roleSet.size() > 0) {
				return roleSet;
			} else {
				return Collections.EMPTY_SET;
			}
		} catch (Exception re) {
			logger.error("in getRoles"+re, re);
			throw re;
		}

	}

	@Override
	public void updateFailAttempts(String username) {
		
		Session session = manager.unwrap(org.hibernate.Session.class);
		
		int maxAttempts = Integer.parseInt(MAX_ATTEMPTS);

		UserAttempts user = getUserAttempts(username);
		if (user == null) {
			if (isUserExists(username)) {
				// if no record, insert a new
				UserAttempts userAttempts = new UserAttempts();
				userAttempts.setAttempts(1);
				userAttempts.setUsername(username);
				userAttempts.setLastModified(new Date());
				session.persist(userAttempts);
			}
		} else {

			if (isUserExists(username)) {
				// update attempts count, +1
				int count = user.getAttempts();
				count = count + 1 ; 
				user.setAttempts(count);
				user.setLastModified(new Date());
				session.update(user);
			}
			
			if (user.getAttempts() + 1 >= maxAttempts) {
				// locked user
				User usr = findByUserName(username);
				usr.setAccountNotLocked(false);
				usr.setUpdated(new Date());
				session.update(usr);
			}

		}
	}

	@Override
	public UserAttempts getUserAttempts(String username) {
		// TODO Auto-generated method stub
		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			Criteria criteria = session.createCriteria(UserAttempts.class);
			criteria.add(Restrictions.eq("username",username ));

			@SuppressWarnings("unchecked")
			List<UserAttempts> users = criteria.list();

			if (users.size() > 0) {
				UserAttempts user = users.get(0);
				return user;
			} else {
				return null;
			}
		} catch (Exception re) {
			logger.error("in getUserAttempts:"+re, re);
			throw re;
		}
	}

	@Override
	public void resetFailAttempts(String username) {
		// TODO Auto-generated method stub

		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			Criteria criteria = session.createCriteria(UserAttempts.class);
			criteria.add(Restrictions.eq("username",username ));

			@SuppressWarnings("unchecked")
			List<UserAttempts> users = criteria.list();

			if (users.size() > 0) {
				UserAttempts user = users.get(0);
				user.setAttempts(0);
				user.setLastModified(new Date());
				session.update(user);
			} else {
				return ;
			}
		} catch (Exception re) {
			logger.error("in getUserAttempts:"+re, re);
			throw re;
		}

	}

	@Override
	public boolean isUserExists(String username) {
		// TODO Auto-generated method stub
		boolean result = false;

		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("username",username));
			criteria.setProjection(Projections.rowCount());

			long count  = (Long) criteria.uniqueResult();

			if (count > 0) {
				result = true;				
			} else {
				return result;
			}
		} catch (Exception re) {
			logger.error("in isUserExists:"+re, re);
			throw re;
		}

		return result;
	}

	@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			Criteria criteria = session.createCriteria(User.class);
			criteria.add( Restrictions.eq("emailid",email ) );

			@SuppressWarnings("unchecked")
			List<User> users = criteria.list();


			if (users.size() > 0) {
				User user = users.get(0);
				logger.debug("User with Roles :"+user.getRoles());
				return user;
			} else {
				return null;
			}
		} catch (Exception re) {
			logger.error("user", re);
			throw re;
		}
	}
}
