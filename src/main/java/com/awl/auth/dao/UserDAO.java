package com.awl.auth.dao;

import java.util.Set;

import com.awl.auth.model.Role;
import com.awl.auth.model.User;
import com.awl.auth.model.UserAttempts;
import com.awl.common.dao.GenericDAO;

public interface UserDAO extends GenericDAO<User> {
	public User findByUserName(String username);
	public Set<Role> getRoles(Integer id);
	
	public void updateFailAttempts(String username);
	public UserAttempts getUserAttempts(String username);
	public void resetFailAttempts(String username);
	public boolean isUserExists(String username) ;
	public User findUserByEmail(String email);
}