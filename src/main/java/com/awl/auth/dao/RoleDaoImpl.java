package com.awl.auth.dao;

import java.util.Collections;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.awl.auth.model.Function;
import com.awl.auth.model.Role;
import com.awl.common.dao.GenericDAOImpl;

@Component("roleDao")
public class RoleDaoImpl extends GenericDAOImpl<Role> implements RoleDao {
	
	private static final Logger logger = LoggerFactory
	.getLogger(RoleDaoImpl.class);

	@PersistenceContext
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	@Override
	public Set<Function> getFunctions(Role r) {

			/*logger.debug("User instance");*/
			try {
				
				Session session = manager.unwrap(org.hibernate.Session.class);
				Role role = (Role) session.get(Role.class,  r.getId());
				Set<Function> funcSet = role.getFunctions();
				
		
				if (funcSet.size() > 0) {
					return funcSet;
				} else {
					return Collections.EMPTY_SET;
				}
			} catch (RuntimeException re) {
				logger.error("in getFunctions"+re, re);
				throw re;
			}

	}



	



}
