package com.awl.auth.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.awl.auth.model.PasswordResetToken;
import com.awl.auth.model.User;
import com.awl.common.dao.GenericDAOImpl;

@Component("passwordResetTokenDAO")
public class PasswordResetTokenDAOImpl extends GenericDAOImpl<PasswordResetToken>  implements PasswordResetTokenDAO {
	private static final Logger logger = LoggerFactory
			.getLogger(PasswordResetTokenDAOImpl.class);

	@PersistenceContext
	private EntityManager manager;

	@Override
	public PasswordResetToken getPasswordResetToken(String token) {
		// TODO Auto-generated method stub
		try {

			Session session = manager.unwrap(org.hibernate.Session.class);
			Criteria criteria = session.createCriteria(PasswordResetToken.class);
			criteria.add(Restrictions.eq("token",token ));

			@SuppressWarnings("unchecked")
			List<PasswordResetToken> prtList = criteria.list();



			/*List<User> users = manager
					.createQuery("SELECT c FROM User c WHERE c.login LIKE :login")
					.setParameter("login", username)
					.getResultList();*/
			if (prtList.size() > 0) {
				PasswordResetToken prt = prtList.get(0);
				logger.debug("PasswordResetToken with user :"+prt.getUser());
				return prt;
			} else {
				return null;
			}
		} catch (Exception re) {
			logger.error("PasswordResetToken", re);
			throw re;
		}
	}


}
