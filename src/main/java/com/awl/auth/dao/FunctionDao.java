package com.awl.auth.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.awl.auth.model.Function;
import com.awl.auth.model.FunctionGroup;
import com.awl.auth.model.Matcher;
import com.awl.common.dao.GenericDAO;

@Component("functionDao")
public interface FunctionDao extends GenericDAO<Function> {

	public List<Matcher> getUrlMatchers();

	public FunctionGroup getFunctionsGroup(int i);

}
