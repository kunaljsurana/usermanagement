package com.awl.auth.dao;

import com.awl.auth.model.PasswordResetToken;
import com.awl.common.dao.GenericDAO;

public interface PasswordResetTokenDAO extends GenericDAO<PasswordResetToken> {

	PasswordResetToken getPasswordResetToken(String token);
}