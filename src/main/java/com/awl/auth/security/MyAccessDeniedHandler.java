package com.awl.auth.security;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

public class MyAccessDeniedHandler implements AccessDeniedHandler{

	private static final Logger logger = LoggerFactory
			.getLogger(MyAccessDeniedHandler.class);
	
	@Override
	public void handle(HttpServletRequest paramHttpServletRequest,
			HttpServletResponse paramHttpServletResponse,
			AccessDeniedException paramAccessDeniedException)
			throws IOException, ServletException {
		logger.error("MyAccessDeniedHandler : Access is Denied. Exception is "+paramAccessDeniedException,paramAccessDeniedException);
		String contextPath =  paramHttpServletRequest.getContextPath();
		paramHttpServletResponse.sendRedirect(contextPath + "/403");
		
	}
	
	

}
