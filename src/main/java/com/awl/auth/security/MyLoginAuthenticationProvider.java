package com.awl.auth.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.awl.auth.dao.UserDAO;
import com.awl.auth.model.UserAttempts;

@Component("authenticationProvider")
public class MyLoginAuthenticationProvider extends DaoAuthenticationProvider {
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	@Qualifier("myUserDetailsService")
	@Override
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		// TODO Auto-generated method stub
		super.setUserDetailsService(userDetailsService);
	}
	
	
	
	
	@Autowired
	@Qualifier("passwordEncoder")
	@Override
	public void setPasswordEncoder(Object passwordEncoder) {
		// TODO Auto-generated method stub
		super.setPasswordEncoder(passwordEncoder);
	}





	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		try {

			Authentication auth = super.authenticate(authentication);
				
			//if reach here, means login success, else an exception will be thrown
			//reset the user_attempts
			userDAO.resetFailAttempts(authentication.getName());
				
			return auth;
				
		  } catch (BadCredentialsException e) {	
				
			//invalid login, update to user_attempts
			  userDAO.updateFailAttempts(authentication.getName());
			throw e;
				
		  } catch (LockedException e){
				
			//this user is locked!
			String error = "";
			UserAttempts userAttempts = 
					userDAO.getUserAttempts(authentication.getName());
			
	               if(userAttempts!=null){
				Date lastAttempts = userAttempts.getLastModified();
				error = "User account is locked! Last Attempt for Username : " 
	                           + authentication.getName() + ":" + lastAttempts;
			}else{
				error = e.getMessage();
			}
				
		  throw new LockedException(error);
		}
	}
	

	
	
	
}
