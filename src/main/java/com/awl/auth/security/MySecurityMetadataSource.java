package com.awl.auth.security;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.ExpressionBasedFilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.StringUtils;

import com.awl.auth.model.Matcher;
import com.awl.auth.service.FunctionService;


public class MySecurityMetadataSource implements FilterInvocationSecurityMetadataSource, InitializingBean {

	@Autowired
	private FunctionService functionService;


	private SecurityExpressionHandler<FilterInvocation> expressionHandler;

	private FilterInvocationSecurityMetadataSource delegate;
	
	public MySecurityMetadataSource(
			SecurityExpressionHandler<FilterInvocation> expressionHandler) {
		super();
		this.expressionHandler = expressionHandler;
	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		// TODO Auto-generated method stub
		return this.delegate.getAllConfigAttributes();
	}

	@Override
	public Collection<ConfigAttribute> getAttributes(Object object)
			throws IllegalArgumentException {
		Collection<ConfigAttribute> coll = this.delegate.getAttributes(object);
		if(coll == null)
		{
			loadResourceDefine();
			return this.delegate.getAttributes(object);
		}
		
		return coll;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return this.delegate.supports(clazz);
	}

	private void loadResourceDefine() {  
		List<Matcher> matcherList = functionService.getUrlMatchers();
		LinkedHashMap<RequestMatcher, Collection<ConfigAttribute>> requestMap = new LinkedHashMap<RequestMatcher, Collection<ConfigAttribute>>(matcherList.size());

		System.out.println("loading URL and Role mapping :"+matcherList);
		for (Matcher matcher : matcherList) {  
			//To access a package for Spring security Object making
			List<ConfigAttribute> configAttributes = SecurityConfig.createList(hasAnyRole(matcher.getRoleInfo()));  
			AntPathRequestMatcher reqMatcher = new AntPathRequestMatcher(matcher.getName());
			requestMap.put(reqMatcher, configAttributes);  
		}  

		this.delegate = new ExpressionBasedFilterInvocationSecurityMetadataSource(requestMap, expressionHandler);
	}

	private static String hasAnyRole(String... authorities) {
		String anyAuthorities = StringUtils.arrayToDelimitedString(authorities, "','");
		return "hasAnyRole('" + anyAuthorities + "')";
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

		loadResourceDefine();
	}  

}
