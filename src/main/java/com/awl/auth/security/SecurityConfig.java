package com.awl.auth.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.awl.auth.model.Matcher;
import com.awl.auth.service.FunctionService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory
			.getLogger(SecurityConfig.class);


	@Autowired
	private FunctionService functionService;

	@Autowired
	@Qualifier("authenticationProvider")
	AuthenticationProvider authenticationProvider;

	DefaultWebSecurityExpressionHandler expressionHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(authenticationProvider);
	}

	@Bean
	public DefaultWebSecurityExpressionHandler getWebSecurityExpressionHandler()
	{
		expressionHandler = new DefaultWebSecurityExpressionHandler();
		return expressionHandler;
	}


	@Bean
	public FilterInvocationSecurityMetadataSource getSecurityMetadataSource(){

		if(expressionHandler==null)
		{
			getWebSecurityExpressionHandler();
		}
		MySecurityMetadataSource mySecurityMetadataSource = new MySecurityMetadataSource(expressionHandler);

		return mySecurityMetadataSource;
	}

	@Bean
	@Autowired 
	public FilterSecurityInterceptor  getSecurityFilter() throws Exception {
		// TODO Auto-generated method stub
		FilterSecurityInterceptor mySecurityFilter = new FilterSecurityInterceptor();
		mySecurityFilter.setAuthenticationManager(authenticationManagerBean());
		mySecurityFilter.setAccessDecisionManager(defaultAccessDecisionManager());
		mySecurityFilter.setSecurityMetadataSource(getSecurityMetadataSource());
		mySecurityFilter.afterPropertiesSet(); 
		return mySecurityFilter;
	}

	@Bean()
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public AffirmativeBased defaultAccessDecisionManager(){
		List<AccessDecisionVoter<? extends Object>> decisionVoters = new ArrayList<>();
		WebExpressionVoter expressionVoter = new WebExpressionVoter();
		if(expressionHandler==null)
		{
			getWebSecurityExpressionHandler();
		}
		expressionVoter.setExpressionHandler(expressionHandler);
		decisionVoters.add(expressionVoter);		
		return new AffirmativeBased(decisionVoters);
	}

	@Bean
	public AccessDeniedHandler getAccessDeniedHandler() {
		// TODO Auto-generated method stub
		MyAccessDeniedHandler myHandler = new MyAccessDeniedHandler();
		return myHandler;
	}


	@Bean(name = "passwordEncoder")
	public PasswordEncoder getPasswordEncoder() {
		// TODO Auto-generated method stub
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}



	private CsrfTokenRepository csrfTokenRepository() {

		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setSessionAttributeName("_csrf");
		return repository;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// This is here to ensure that the static content (JavaScript, CSS, etc)
		// is accessible from the login page without authentication
		web.ignoring().antMatchers("/resources/**","/");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().csrfTokenRepository(csrfTokenRepository());



		http.authorizeRequests()	
		.antMatchers("/login**/**","/forgetPwd").permitAll();	

		http.addFilter(getSecurityFilter());

		List<Matcher> matcherList = functionService.getUrlMatchers();
		for(Matcher m : matcherList){
			logger.debug("URL :"+m.getName()+" and their roles :"+new HashSet<String>(Arrays.asList(m.getRoleInfo())));
			http.authorizeRequests().antMatchers(m.getName()).hasAnyRole(m.getRoleInfo());
		}

		http.authorizeRequests()

		.and()
		// access-denied-page: this is the page users will be
		// redirected to when they try to access protected areas.
		.exceptionHandling()
		.accessDeniedHandler(getAccessDeniedHandler())
		//.accessDeniedPage("/403")
		.and()

		// The intercept-url configuration is where we specify what
		// roles are allowed access to what areas.
		// We specifically force the connection to https for all the
		// pages, although it could be sufficient
		// just on the login page. The access parameter is where the
		// expressions are used to control which
		// roles can access specific areas. One of the most important
		// things is the order of the intercept-urls,
		// the most catch-all type patterns should at the bottom of the
		// list as the matches are executed
		// in the order they are configured below. So /** (anyRequest())
		// should always be at the bottom of the list.


		// This is where we configure our login form.
		// login-page: the page that contains the login screen
		// login-processing-url: this is the URL to which the login form
		// should be submitted
		// default-target-url: the URL to which the user will be
		// redirected if they login successfully
		// authentication-failure-url: the URL to which the user will be
		// redirected if they fail login
		// username-parameter: the name of the request parameter which
		// contains the username
		// password-parameter: the name of the request parameter which
		// contains the password
		.formLogin()
		.loginPage("/login")
		.defaultSuccessUrl("/index")
		.failureUrl("/login?error")
		.usernameParameter("username")
		.passwordParameter("password")
		.and()

		// This is where the logout page and process is configured. The
		// logout-url is the URL to send
		// the user to in order to logout, the logout-success-url is
		// where they are taken if the logout
		// is successful, and the delete-cookies and invalidate-session
		// make sure that we clean up after logout
		.logout()
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		.logoutSuccessUrl("/login?logout")
		.deleteCookies("JSESSIONID")
		.invalidateHttpSession(true)
		.and()

		.authorizeRequests()				
		.anyRequest()
		.authenticated()
		.and()


		// The session management is used to ensure the user only has
		// one session. This isn't
		// compulsory but can add some extra security to your
		// application.
		.sessionManagement().sessionFixation().newSession().invalidSessionUrl("/login?sessionExpired").maximumSessions(1).expiredUrl("/login?expired").maxSessionsPreventsLogin(true);


	}

	/*@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.inMemoryAuthentication().withUser("username").password("password")
				.authorities("USER_ADMIN");
	}*/
}
