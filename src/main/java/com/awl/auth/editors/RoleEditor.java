package com.awl.auth.editors;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import com.awl.auth.model.Role;

public class RoleEditor extends CustomCollectionEditor  {
	
	private static final Logger logger = LoggerFactory
			.getLogger(RoleEditor.class);
	

	private HashMap<String, Role> roleMap;
	
	@SuppressWarnings("rawtypes")
	public RoleEditor(Class<? extends Collection> collectionType,HashMap<String, Role> roleMap) {
		super(collectionType);
		this.roleMap = roleMap;
	}
	
	@Override
    protected Object convertElement(Object element) {
        
        if (element instanceof Role) {
        	logger.debug("Converting from Role to Role: " + element);
			return element;
		}
		if (element instanceof String) {
			Role role = roleMap.get(element);
			logger.debug("Looking up Role for id " + element + ": " + role);
			return role;
		}
        return null;
    }

}
