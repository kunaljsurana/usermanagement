package com.awl.auth.editors;

import java.beans.PropertyEditorSupport;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.awl.auth.model.FunctionGroup;

public class FunctionGroupEditor extends PropertyEditorSupport  {

	private static final Logger logger = LoggerFactory
			.getLogger(FunctionGroupEditor.class);


	private HashMap<String, FunctionGroup> functionGroupMap;

	public FunctionGroupEditor(HashMap<String, FunctionGroup> functionGroupMap) {
		this.functionGroupMap = functionGroupMap;
		logger.debug("FunctionGroupEditor :"+functionGroupMap);
	}










	public String getAsText() {


		FunctionGroup value = ( FunctionGroup ) getValue();
		logger.debug("getAsText :"+value);
		return ""+value;
	}


	public void setAsText( String text ) throws IllegalArgumentException {
		logger.debug("setAsText :"+text);
		FunctionGroup functionGrp = functionGroupMap.get(text);
		logger.debug("Looking up FunctionGroup for id " + text + ": " + functionGrp);
		setValue(functionGrp);
	}






	/*
	protected Object convertElement(Object element) {

        if (element instanceof FunctionGroup) {
        	logger.debug("Converting from FunctionGroup to FunctionGroup: " + element);
			return element;
		}
		if (element instanceof String) {
			FunctionGroup function = functionGroupMap.get(element);
			logger.debug("Looking up FunctionGroup for id " + element + ": " + function);
			return function;
		}
        return null;
    }*/

}
