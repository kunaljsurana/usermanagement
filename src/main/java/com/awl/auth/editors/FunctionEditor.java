package com.awl.auth.editors;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import com.awl.auth.model.Function;

public class FunctionEditor extends CustomCollectionEditor  {
	
	private static final Logger logger = LoggerFactory
			.getLogger(FunctionEditor.class);
	

	private HashMap<String, Function> functionMap;
	
	@SuppressWarnings("rawtypes")
	public FunctionEditor(Class<? extends Collection> collectionType,HashMap<String, Function> functionMap) {
		super(collectionType);
		this.functionMap = functionMap;
	}
	
	@Override
    protected Object convertElement(Object element) {
        
        if (element instanceof Function) {
        	logger.debug("Converting from Function to Function: " + element);
			return element;
		}
		if (element instanceof String) {
			Function function = functionMap.get(element);
			logger.debug("Looking up Function for id " + element + ": " + function);
			return function;
		}
        return null;
    }

}
