package com.awl.displaytag;

import org.displaytag.decorator.TableDecorator;

import com.awl.auth.model.Function;
import com.awl.auth.model.FunctionGroup;
import com.awl.auth.model.Role;
import com.awl.auth.model.User;



public class DisplayTagDecorator extends TableDecorator{
	
	
	
	
	public String getEditUser(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		User record =  (User) getCurrentRowObject();
		url =  "<a href=\""+context+"/users/edit/"+record.getId()+"\" >Edit</a>";
		return url;
	}
	
	public String getDisabledUser(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		User record =  (User) getCurrentRowObject();
		url =  "<a href=\""+context+"/users/disable/"+record.getId()+"\" onclick=\"return confirm('Are you sure, You want to disable this user?')\">Disable</a>";
		return url;
	}
	
	public String getEditRole(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		Role record =  (Role) getCurrentRowObject();
		url =  "<a href=\""+context+"/roles/edit/"+record.getId()+"\" >Edit</a>";
		return url;
	}
	
	public String getDeleteRole(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		Role record =  (Role) getCurrentRowObject();
		url =  "<a href=\""+context+"/roles/remove/"+record.getId()+"\" onclick=\"return confirm('Are you sure, You want to delete this role?')\">Delete</a>";
		return url;
	}
	
	public String getEditFunctionGroup(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		FunctionGroup record =  (FunctionGroup) getCurrentRowObject();
		url =  "<a href=\""+context+"/functionGroups/edit/"+record.getId()+"\" >Edit</a>";
		return url;
	}
	
	public String getDeleteFunctionGroup(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		FunctionGroup record =  (FunctionGroup) getCurrentRowObject();
		url =  "<a href=\""+context+"/functionGroups/remove/"+record.getId()+"\" onclick=\"return confirm('Are you sure, You want to delete this Function Group?')\">Delete</a>";
		return url;
	}
	
	public String getEditFunction(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		Function record =  (Function) getCurrentRowObject();
		url =  "<a href=\""+context+"/functions/edit/"+record.getId()+"\" >Edit</a>";
		return url;
	}
	
	public String getDeleteFunction(){
		String context = getPageContext().getServletContext().getContextPath();
		String url=null;
		Function record =  (Function) getCurrentRowObject();
		url =  "<a href=\""+context+"/functions/remove/"+record.getId()+"\" onclick=\"return confirm('Are you sure, You want to delete this Function?')\">Delete</a>";
		return url;
	}

}
