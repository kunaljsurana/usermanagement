package com.awl.common.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public abstract class GenericDAOImpl<T> implements GenericDAO<T> {

	@PersistenceContext
	private EntityManager manager;

	private static final Logger logger = LoggerFactory
			.getLogger(GenericDAOImpl.class);

	protected Class<T> type;

	@SuppressWarnings("unchecked")
	public GenericDAOImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<T>) pt.getActualTypeArguments()[0];
	}

	public T getById(String id) {
		logger.debug("getting  instance with id: " + id);
		try {
			logger.debug("get successful");

			T type1 = (T) manager.find(type, id);
			if (type1 == null) {

				return null;
			}
			return type1;
		} catch (RuntimeException re) {
			logger.error("get failed", re);
			throw re;
		}
	}

	public T getById(int id) {
		logger.debug("getting RouterEntity instance with id: " + id);
		try {
			logger.debug("get successful");

			T type1 = (T) manager.find(type, id);
			if (type1 == null) {

				return null;
			}
			return type1;
		} catch (RuntimeException re) {
			logger.error("get failed", re);
			throw re;
		}
	}
	
	

	public void remove(int r) {
		try {
			T type1 = (T) getById(r);
			manager.remove(type1);
			logger.debug("getById");
		} catch (RuntimeException re) {
			logger.error("remove failed", re);
			throw re;
		}

	}
	

	public void remove(String r) {
		try {
			T type1 = (T) getById(r);
			manager.remove(type1);
			logger.debug("getById");
		} catch (RuntimeException re) {
			logger.error("remove failed", re);
			throw re;
		}

	}
	
	public void delete(Long arg0) {
		// TODO Auto-generated method stub

	}

	/*public T saveAndFlush(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	
	public <S extends T> S saveAndFlush(S arg0) {
	// TODO Auto-generated method stub
	return null;
}

	public void delete(T arg0) {
	// TODO Auto-generated method stub

	}

	/*public void delete(Object arg0) {
		// TODO Auto-generated method stub

	}*/

	public void deleteAllInBatch() {
		// TODO Auto-generated method stub

	}

	public void deleteInBatch(Iterable<T> arg0) {
		// TODO Auto-generated method stub

	}

	

	public List<T> findAll(Iterable<Long> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public void flush() {
		// TODO Auto-generated method stub

	}

	public T getOne(Long arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public <S extends T> List<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	

	public void delete(Iterable<? extends T> arg0) {
		// TODO Auto-generated method stub

	}
	
	

	@Override
	public List<T> findAll(Sort paramSort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<T> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	public void deleteAll() {
		// TODO Auto-generated method stub

	}

	public boolean exists(Long arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public T findOne(Long arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public <S extends T> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	

	public void add(T r) {
		logger.debug("persisting Area instance");
		try {
			manager.persist(r);
			logger.debug("persist successful");
		} catch (RuntimeException re) {
			logger.error("persist failed", re);
			throw re;
		}
	}
	
	

	public void update(T r) {
		logger.debug("merging Area instance");
		try {
			manager.merge(r);
			logger.debug("merge successful");
		} catch (RuntimeException re) {
			logger.error("merge failed", re);
			throw re;
		}

	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		// TODO Auto-generated method stub
		Session session = manager.unwrap(org.hibernate.Session.class);
		Criteria criteria = session.createCriteria(type);
		
	
		return  criteria.list();
	}

	public long count() {
		// TODO Auto-generated method stub
		Session session = manager.unwrap(org.hibernate.Session.class);
		Criteria criteria = session.createCriteria(type);
		criteria.setProjection(Projections.rowCount());
	
		return (Long) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll(int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		
		Session session = manager.unwrap(org.hibernate.Session.class);
		Criteria criteria = session.createCriteria(type);
		if(firstResult <= 0 || maxResults <= 0 )
		{
			
			List<T> list = criteria.list();
			return  list ;
		}
		List<T> list = criteria.setFirstResult(firstResult)
				.setMaxResults(maxResults).list();
		
		return  list;
	}

}
