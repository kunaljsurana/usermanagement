package com.awl.common.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GenericDAO<T> extends JpaRepository<T, Long> {
	public void add(T r);

	public void update(T r);

	public T getById(int id);

	public void remove(int id);

	public List<T> findAll(int firstResult, int maxResults);

	public T getById(String id);

	public long count();
	
	public void remove(String r) ;
}
