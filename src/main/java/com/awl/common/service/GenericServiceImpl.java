package com.awl.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.awl.common.dao.GenericDAO;

public class GenericServiceImpl<T> implements GenericService<T> {

	@Autowired
	private GenericDAO<T> genericDAO;

	public void update(T p) {
		this.genericDAO.update(p);
	}

	public T getById(int id) {
		return this.genericDAO.getById(id);
	}

	public void add(T r) {
		this.genericDAO.add(r);
	}

	public void remove(int r) {
		this.genericDAO.remove(r);
	}

	public T getById(String id) {
		return this.genericDAO.getById(id);
	}

	public long count() {
		return this.genericDAO.count();
	}

	public void remove(String r) {
		this.genericDAO.remove(r);
	}

	@Override
	public List<T> findAll(int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		List<T> list =  genericDAO.findAll(firstResult, maxResults);
		return list;
	}
	

}
