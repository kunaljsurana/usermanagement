package com.awl.common.service;

import java.util.List;


public interface GenericService<T> {
	public void add(T r);
	public void update(T r);
	public T getById(int id);
	public void remove(int r);
	public void remove(String r);
	public T getById(String id);
	public long count();
	public List<T> findAll(int firstResult, int maxResults);
}
