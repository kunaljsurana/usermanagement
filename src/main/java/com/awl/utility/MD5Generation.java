package com.awl.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MD5Generation {

	
	 private static final Logger logger = LoggerFactory
	 .getLogger(MD5Generation.class);
	 

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String convertToMD5(String text) {
		MessageDigest md;
		byte[] md5hash = new byte[32];
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes("iso-8859-1"), 0, text.length());
			md5hash = md.digest();
		} catch (NoSuchAlgorithmException ex) {
			logger.info("Exception in convertToMD5 :" + ex , ex);
		} catch (UnsupportedEncodingException ex) {
			logger.info("Exception in convertToMD5 :" + ex , ex);
		}
		return convertToHex(md5hash);
	}

	public static String doMask(int in, int fill, char tofillchar) {
		boolean negative = false;
		int value, len = 0;

		if (in >= 0) {
			value = in;
		} else {
			negative = true;
			value = -in;
			in = -in;
			len++;
		}

		if (value == 0) {
			len = 1;
		} else {
			for (; value != 0; len++) {
				value /= 10;
			}
		}

		StringBuilder sb = new StringBuilder();

		if (negative) {
			sb.append('-');
		}

		for (int i = fill; i > len; i--) {
			sb.append(tofillchar);
		}

		sb.append(in);

		return sb.toString();
	}

	public static String last4digit(String cardNumber) {
		int len = cardNumber.length();
		return cardNumber.substring(len - 4);
	}

	public static String maskCardNumber(String cardNumber) {
		return doMask(Integer.valueOf(last4digit(cardNumber)), 14, '*');
	}

	public static java.sql.Timestamp dateToTimestamp() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}

	public static String dateToString() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
		return sdf.format(cal.getTime());
	}

}
