package com.awl.utility;


import java.io.IOException;
import java.util.Properties;





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.awl.auth.model.User;


@Service("springMailSender")
public class MailSender {
	private static final Logger logger = LoggerFactory
			.getLogger(MailSender.class);

	@Autowired
	private SimpleMailMessage emailTemplate;

	@Autowired
	private JavaMailSenderImpl mailSender;
	
	
	@Value(value = "#{'${email.body.resetTokenMessage}'}")
	private String resetTokenMessage;
	
	@Value(value = "#{'${email.regards}'}")
	private String regardsMessage;
	

	public void setEmailTemplate(SimpleMailMessage emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	
	
	public boolean sendResetTokenEmail(String toEmail, String contextPath , User user, String token) {
		
		boolean flag = true;

		try
		{
		String url = contextPath + "/forgetPwd/changePassword?id=" + user.getId() + "&token=" + token;
	    SimpleMailMessage email = new SimpleMailMessage(emailTemplate);
	    email.setTo(toEmail);
	    email.setSubject("Reset Password");
	    email.setText(resetTokenMessage + "\n" + url + "\n\n" + regardsMessage);
	    mailSender.send(email);
		}catch(Exception e)
		{
			flag = false;
			logger.error("Exception in sendResetTokenEmail:",e);
		}
		
	    return flag;
	
	}
	
	
	

}

